# CorPower Ocean - Independent Visualization HMI

---

## Table of Contents

- [1. Overview](#1-overview)
- [2. Installation](#2-installation)
  - [2.1. Prerequisites](#21-prerequisites)
  - [2.2. Installation](#22-installation)
    - [2.2.1. Docker (recommended)](#221-docker-recommended)
    - [2.2.2. Manual](#222-manual)
- [3. Deployment](#3-deployment)
  - [3.1. Prerequisites](#31-prerequisites)
  - [3.2. Deployment](#32-deployment)
    - [3.2.1. Docker (recommended)](#321-docker-recommended)
    - [3.2.2. Manual](#322-manual)
- [4. Development](#4-development)
  - [4.1. Prerequisites/Installation](#41-prerequisitesinstallation)
  - [4.2. Running the application](#42-running-the-application)
    - [4.2.1. Front End](#421-front-end)
    - [4.2.2. Back End](#422-back-end)
  - [4.3. Running the tests](#43-running-the-tests)
- [5. Changing the ports](#5-changing-the-ports)
  - [5.1. Docker (recommended)](#51-docker-recommended)
    - [5.1.1 Back End](#511-back-end)
    - [5.1.2 Front End](#512-front-end)
  - [5.2. Manual](#52-manual)
    - [5.2.1. Back End](#521-back-end)
    - [5.2.2 Front End](#522-front-end)
- [6. Structure](#6-structure)
  - [6.1. Front end](#61-front-end)
  - [6.2. Back end](#62-back-end)

## 1. Overview

This is the documentation for the CorPower Ocean - Independent Visualization HMI. It is a web application that is used to visualize the data from C4.

It uses an API for the backend (using FastAPI library) and a web application for the front end (using React framework).

The backend is written in Python and the front end is written in JavaScript.

## 2. Installation

- ### 2.1. Prerequisites

  - Docker (recommended): Ensure you have Docker installed. You can download it from [docker.com](https://www.docker.com/products/docker-desktop).

  If you don't plan on using docker you will need to install the following:
  - Node.js: Ensure you have Node.js installed. You can download it from [nodejs.org](https://nodejs.org/en/download).
  - Python (>=3.10): Ensure you have Python installed. You can download it from [python. org](https://www.python.org/downloads/).

- ### 2.2. Installation

  - #### 2.2.1. Docker (recommended)

    Clone the repository in the folder of your choice, then build the docker image

    ```bash
    git clone <REPOSITORY_URL>
    cd /c4_independent_hmi
    docker-compose build
    ```

  - #### 2.2.2. Manual

    Clone the repository in the folder of your choice, then install the required packages

    ```bash
    git clone <REPOSITORY_URL>
    cd /c4_independent_hmi
    ```

    - Front end:
      Install npm and the packages

      ```bash
      cd /c4_independent_hmi/client
      npm install
      npm install -g serve
      npm run build
      ```

    - Back end:
      Install the required packages

      ```bash
      cd /server
      pip install -r requirements.txt
      ```

    Depending on your python installation you might have to use `python -m pip install`instead or `py -m pip install` before `pip install`

## 3. Deployment

- ### 3.1. Prerequisites

  Check [Installation](#2-installation)

- ### 3.2. Deployment

    If you want to change the ports read [5. Changing the Ports](#5-changing-the-ports).

  - #### 3.2.1. Docker (recommended)

    ```bash
    cd /c4_independent_hmi
    docker-compose up
    ```

    It will start the front end and the back end, you can access the front end at <http://localhost:8282> and the back end at <http://localhost:8000>

  - #### 3.2.2. Manual

    - #### 3.2.2.1. Front End

      ```bash
      cd /c4_independent_hmi/client
      serve -s build
      ```

      You can access the front end at <http://localhost:3000>

      If running on windows you might get into an error regarding the execution of a script not being authorized:

      ```powershell
      serve : File C:\Users\UserName\AppData\Roaming\npm\serve.ps1 be loaded. The file C:\Users\UserName\AppData\Roaming\npm\serve.ps1 is not digitally signed. You cannot run this script on the current system. For more information about running scripts and setting execution policy, see about_Execution_Policies at https:/go.microsoft.com/fwlink/?LinkID=135170.
        At line:1 char:1
        + serve -s build
        + ~~~~~
            + CategoryInfo          : SecurityError: (:) [], PSSecurityException
            + FullyQualifiedErrorId : UnauthorizedAccess
      ```

      To fix this:

      - First check the permissions

        ```powershell
        Get-ExecutionPolicy -List
        ```

        You should get something like this:

        ```powershell
                Scope ExecutionPolicy
                ----- ---------------
        MachinePolicy       Undefined
           UserPolicy       Undefined
              Process       Undefined
          CurrentUser       AllSigned
         LocalMachine       Undefined
        ```

      - Then give CurrentUser RemoteSigned permission like this:

        ```powershell
        set-ExecutionPolicy RemoteSigned -Scope CurrentUser
        ```

      - Then check again the permissions

        ```powershell
        Get-ExecutionPolicy -List
        ```

        You should get something like this:

        ```powershell
                Scope ExecutionPolicy
                ----- ---------------
        MachinePolicy       Undefined
           UserPolicy       Undefined
              Process       Undefined
          CurrentUser    RemoteSigned
         LocalMachine       Undefined
        ```

    - #### 3.2.2.2. Back End

      ```bash
      cd /c4_independent_hmi/server
      uvicorn BackendAPI:app
      ```

      You can access the back end at <http://localhost:8000>

## 4. Development

- ### 4.1. Prerequisites/Installation

  Check [Installation](#2-installation)

- ### 4.2. Running the application

    If you want to change the ports read [Development/ Changing the ports](#5-changing-the-ports).

  - #### 4.2.1. Front End

    ```bash
    cd /c4_independent_hmi/client
    npm start run
    ```

    This will start the front end, you can access it at <http://localhost:3000>

  - #### 4.2.2. Back End

    ```python
    cd /c4_independent_hmi/server
    uvicorn BackendAPI:app --reload
    ```

    This will start the back end, you can access it at <http://localhost:8000>

- ### 4.3. Running the tests

  Currently there are no tests implemented

## 5. Changing the ports

- ### 5.1. Docker (recommended)

  - #### 5.1.1 Back End

    - Change in [`./docker-compose.yml`](./docker-compose.yml)

      ```yaml
      backend:
        build: ./server
        ports:
          - "<YOUR_NEW_PORT>:8000"
        restart: unless-stopped
      ```

    - Change in [`./APIsocket.js`](./client/src/APIsocket.js) the following lines to match your configuration:

      ```javascript
      baseURL: 'http://127.0.0.1:<YOUR_NEW_PORT>', // Set the backend API base URL here
      ```

  - #### 5.1.2 Front End

    - Change in [`./docker-compose.yml`](./docker-compose.yml) the following lines to match your configuration:

      ```yaml
      frontend:
        build: ./client
        ports:
          - "<YOUR_NEW_PORT>:3000"
        restart: unless-stopped
      ```

- ### 5.2. Manual

  - #### 5.2.1. Back End

      - Change in [`./APIsocket.js`](./client/src/APIsocket.js) the following line so it matches with the port you will be using for the backend API.

        ```javascript
        baseURL: 'http://127.0.0.1:<YOUR_NEW_PORT>', // Set the backend API base URL here
        ```

      - When running the backend API, you will need to specify the port you want to use. To do so, you will need to run the following command:

        ```bash
        cd /c4_independent_hmi/server
        uvicorn BackendAPI:app --port <YOUR_NEW_PORT>
        ```

        if you are developing you might prefer to use the `--reload` flag to reload the server when you make changes to the code

        ```bash
        cd /c4_independent_hmi/server
        uvicorn BackendAPI:app --reload --port <YOUR_NEW_PORT>
        ```

  - #### 5.2.2 Front End

    - You need to use the following command when running the application:

      ```bash
      cd /c4_independent_hmi/client
      server -s build -l <YOUR_NEW_PORT>
      ```

      if you are developing you might prefer to update [`./client/package.json`](./client/package.json) so the server reloads when you make changes to the code

      Windows:

      ```json
      "scripts": {
        "start": "set PORT=<YOUR_NEW_PORT> && react-scripts start",
      ```

      Linux (Not Tested):

      ```json
      "scripts": {
        "start": "PORT=<YOUR_NEW_PORT> react-scripts start",
      ```

## 6. [Structure](./)

- [`./client/`](./client/) contains the front end of the application
- [`./server/`](./server/) contains the back end of the application
- [`./docker-compose.yml`](./docker-compose.yml) is the docker compose file for the application it reference both `dockerfile` of the backend and the frontend ([`./server/dockerfile`](./server/dockerfile) and [`./client/dockerfile`](./client/dockerfile) respectively)
- [`./README.md`](./README.md) contains the documentation for the application

- ### 6.1. [Front end](./client/)

  - [`./public/`](./client/public/) contains the public files of the application
  - [`./src/`](./client/src/) contains the source files of the application
  - [`./dockerfile`](./client/dockerfile) contains the configuration for the docker image of the front end
  - [`./package.json`](./client/package.json) contains the different packages used by the application

  - #### 6.1.1. [Source code](./client/src/)

    - [`./assets/`](./client/src/assets/) contains the assets of the application (images, default data files, etc.)
    - [`./components/`](./client/src/components/) contains the different react components of the application
    - [`./scenes/`](./client/src/scenes/) contains the different scenes/pages of the application
    - [`./state/`](./client/src/state/) contains the different states of the application (dark/light theme)
    - [`./APIsocket.js`](./client/src/APIsocket.js) This is the file that handles the communication with the back end
    - [`./App.js`](./client/src/App.js) is the main file of the application, it contains the different routes and the different states of the application
    - [`./index.css`](./client/src/index.css) is the main css file of the application, the font and app size are defined here
    - [`./index.js`](./client/src/index.js) is the main file of the web page, it renders the application
    - [`./theme.js`](./client/src/theme.js) is the theme file of the application, it contains the different colors/themes of the application

    - ##### 6.1.1.1. [Assets](./client/src/assets/)

      - [`CPO_Logo_...`](./client/src/assets/CPO_Logo_White_RGB.png) it's the logo displayed on the top left corner of the application switches between the two of them depending on the theme (light/dark)
      - [`DataList_EMPTY.js`](./client/src/assets/DataList_EMPTY.js) it's a fake dataset with the correct formatting for the TreeSelector used to select the signals ref. [Components/TreeSelector](#6112-components)
      - [`event_info.json`](./client/src/assets/event_info.json) it's a file that contains the different information about the different events ref. [Scenes/History/Event](#6113-scenes)

    - ##### 6.1.1.2. [Components](./client/src/components/)

      - [`./EventTables.jsx`](./client/src/components/EventTables.jsx) This component is used to display the different events, it can take an input used as initial value, and it auto updates itself every 30 seconds. It is used in [Scenes/History/Event](#6112-components)

        ```javascript	
        const intervalId = setInterval(() => {
          fetchEventData(); // Fetch report data every 30 seconds
        }, 30000); // 30,000 milliseconds = 30 seconds
        ```

      - [`./FlexBetween.jsx`](./client/src/components/FlexBetween.jsx) it's a component that is used to arrange a component between other components nicely
      - [`./Header.jsx`](./client/src/components/Header.jsx) it's a component that is used to display the header of the pages
      - [`./LineChart.jsx`](./client/src/components/LineChart.jsx) it's a component that is used to display a line chart, it takes as input a dataset and the **name** of the signal to use as X axis (it removes "time" and selected signals to display). It is used in [Scenes/History/Signal](#6112-components)

        ```javascript
        const LineChart = ({ DataSet, X_SignalName }) => {
        const dataPlot = Object.keys(DataSet)
          .filter((signalKey) => signalKey !== X_SignalName) // Exclude "X Axis" signal
          .filter((signalKey) => signalKey !== "time") // Exclude "time" signal
          .map((signalKey) => ({
            x: DataSet[X_SignalName],
            y: DataSet[signalKey],
            type: "scatter",
            mode: "lines",
            name: signalKey,
          }));
          ...
        };
        ```

      - [`./NavBar.jsx`](./client/src/components/Navbar.jsx) is a component that is used to display the navigation bar (it's the bar at the top of the application), it contains some useful information (user and role) and quick actions (theme switching, logout, etc.)
      - [`./RangeDatePicker.jsx`](./client/src/components/RangeDatePicker.jsx) is a component that is used to display a date range picker (it you can select up to the second), it has quick selections (last 1min, 5min, 15min, 30min, 1h, 6h, 12h, 24h). It is used in [Scenes/History/Signal](#6113-scenes) and [Scenes/Report](#6113-scenes)
      - [`./ReportTable.jsx`](./client/src/components/ReportTable.jsx) is a component that is used to display a report table, it can take as input an initial dataset and it auto updates itself every 30 seconds. It is used in [Scenes/Report](#6113-scenes)

        ```javascript
        const intervalId = setInterval(() => {
          fetchReportData(); // Fetch report data every 30 seconds
        }, 30000); // 30,000 milliseconds = 30 seconds
        ```

      - [`./SideBar.jsx`](./client/src/components/SideBar.jsx) is a component that is used to display the side bar (it's the bar at the left of the application), it contains the different links to the different pages of the application
      - [`./ToggleHrLr.jsx`](./client/src/components/ToggleHrLr.jsx) is a component that is used to display a toggle between high rate and low rate. It is used in [Scenes/History/Signal](#6113-scenes)
      - [`./TreeSelector.jsx`](./client/src/components/TreeSelector.jsx) is a component that is used to display a tree selector it loads the available signals in the dataTree format automatically when the component is loaded. It is used in [Scenes/History/Signal](#6113-scenes) and [Scenes/Report](#6113-scenes)
      - [`./UserTable.jsx`](./client/src/components/UserTable.jsx) is a component that is used to display the current user(s) sessions it updates them every 30 minutes, it is used in [Scenes/SysInfo](#6113-scenes)

        ```javascript
        const interval = setInterval(() => {
          fetchUserData();
        }, 1800000); // 30 minutes in milliseconds (30 * 60 * 1000)
        ```

    - ##### 6.1.1.3. [Scenes](./client/src/scenes/)

      Every file in this folder has a parent folder for separation as they are all called `index.jsx`, we will address the files via their parent folder

      - [`./login/`](./client/src/scenes/login/index.jsx) Contains the login page, you can login to the application from there
      - [`./layout/`](./client/src/scenes/layout/index.jsx) Contains the different layouts of the application **ONCE logged in** (every time you access it it checks the stored credentials to improve the security), it adds the [NavBar](#6112-components) and [SideBar](#6112-components) to the page
      - [`./dashboard/`](./client/src/scenes/dashboard/index.jsx) Main page, you land there after login in, contain a variety of important data displayed in different ways **NOT IMPLEMENTED YET**
      - [`./history/signal/`](./client/src/scenes/history/signals/index.jsx) Here you can display High Rate data (limited to 30min), or Low Rate data, you can select the signal you want to display and the time range. It is displayed on [LineChart](#6112-components), you can save it as a `.csv` file
      - [`./history/event/`](./client/src/scenes/history/events/index.jsx) Here is displayed the latest events recorded, the event list is updated at the same rate as the data
      - [`./report/`](./client/src/scenes/report/index.jsx) Here you can either generate a report or record a report. In both case you select the wanted signals and the frequency of the measurements (in minutes), when you want to generate a report you have to additionally select a datetime range
      - [`./settings/`](./client/src/scenes/settings/index.jsx) Contains the different settings of the application, you can change the different settings it concern the data fetching (addresses, users, etc.)
      - [`./sysinfo/`](./client/src/scenes/sysinfo/index.jsx) It is displayed the processes running on the infrastructure (CPC_0, DPC_0, LPC_0) and the ones running on the server backend (with the correct permissions you can Stop, Pause, and restart the processes)

      The other ones are not implemented YET

    - ##### 6.1.1.4. [State](./client/src/state/)

      - [`index.js`](./client/src/state/index.js) Handles the theme switching

- ### 6.2. [Back end](./server/)

  - [`./BackendAPI.py`](./server/BackendAPI.py) This is the main file for the application
  - [`./users.json`](./server/users.json) Contains the users and their access levels for the application
  - [`./requirements.txt`](./server/requirements.txt) Contains the required python packages for the application
  - [`./dockerfile`](./server/dockerfile) This is the docker file for the back end (Image configuration)
  - [``./bus_info_total_ref.json`](./server/bus_info_total_ref.json) This file contains information about the different signals (name, unit, etc.)
