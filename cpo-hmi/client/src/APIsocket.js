// APIsocket.js

import axios from 'axios';

const APIsocket = {
  baseURL: 'http://127.0.0.1:8000', // Set the backend API base URL here

  login: async (username) => {
    const url = `${APIsocket.baseURL}/login`;
    try {
      const params = { username };
      const response = await axios.get(url, { params });
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  createSession: async (username, role, sessionID) => {
    const url = `${APIsocket.baseURL}/session?username=${username}&role=${role}&sessionID=${sessionID}`;
    try {
      const response = await axios.post(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  deleteSession: async (sessionID) => {
    const url = `${APIsocket.baseURL}/session?sessionID=${sessionID}`;
    try {
      const response = await axios.delete(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  getUsers: async () => {
    const url = `${APIsocket.baseURL}/users`;
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  getSignals: async (rate, signalFullNames, startDateTime, endDateTime) => {
    let url = `${APIsocket.baseURL}/signals?rate=${rate}&startDateTime=${startDateTime.replace('+', 'T')}&endDateTime=${endDateTime.replace('+', 'T')}`;
    for (let i = 0; i < signalFullNames.length; i++) {
      url += `&signalFullNames=${signalFullNames[i]}`;
    }
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  getQueryables: async () => {
    const url = `${APIsocket.baseURL}/queryables`;
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  getDataTree: async (rate) => {
    const url = `${APIsocket.baseURL}/dataTree?rate=${rate}`;
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  getReport: async () => {
    const url = `${APIsocket.baseURL}/report`;
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  getReportSettings: async () => {
    const url = `${APIsocket.baseURL}/reportSettings`;
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  setReportSettings: async (report_state, signalFullNames, sampleRate, startDateTime, endDateTime) => {
    let url = `${APIsocket.baseURL}/reportSettings?report_state=${report_state}&sampleRate=${sampleRate}&startDateTime=${startDateTime.replace('+', 'T')}&endDateTime=${endDateTime.replace('+', 'T')}`;
    for (let i = 0; i < signalFullNames.length; i++) {
        url += `&signalFullNames=${signalFullNames[i]}`;
        }
    try {
      const response = await axios.post(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  startReport: async (sampleRate, signalFullNames) => {
    let url = `${APIsocket.baseURL}/report/Start?sampleRate=${sampleRate}`;
    for (let i = 0; i < signalFullNames.length; i++) {
        url += `&signalFullNames=${signalFullNames[i]}`;
        }
    try {
      const response = await axios.post(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  stopReport: async () => {
    const url = `${APIsocket.baseURL}/report/Stop`;
    try {
      const response = await axios.post(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  generateReport: async (sampleRate, signalFullNames, startDateTime, endDateTime) => {
    let url = `${APIsocket.baseURL}/report/GEN?sampleRate=${sampleRate}&startDateTime=${startDateTime.replace('+', 'T')}&endDateTime=${endDateTime.replace('+', 'T')}`;
    for (let i = 0; i < signalFullNames.length; i++) {
        url += `&signalFullNames=${signalFullNames[i]}`;
        }
    try {
      const response = await axios.post(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  getEvents: async () => {
    const url = `${APIsocket.baseURL}/events`;
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },
  getEventsData: async () => {
    const url = `${APIsocket.baseURL}/events/data`;
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  getSettings: async () => {
    const url = `${APIsocket.baseURL}/settings`;
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  setSettings: async (new_HrAPIurl, new_LrAPIurl, new_realTimeUpdatePeriod, new_availableSignalsUpdatePeriod) => {
    const url = `${APIsocket.baseURL}/settings?new_HrAPIurl=${new_HrAPIurl}&new_LrAPIurl=${new_LrAPIurl}&new_realTimeUpdatePeriod=${new_realTimeUpdatePeriod}&new_availableSignalsUpdatePeriod=${new_availableSignalsUpdatePeriod}`;
    try {
      const response = await axios.post(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  getPing: async (ip) => {
    const url = `${APIsocket.baseURL}/ping?ip=${ip}`;
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  getPingInfo: async (ip) => {
    const url = `${APIsocket.baseURL}/ping/info?ip=${ip}`;
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  getSuper: async () => {
    const url = `${APIsocket.baseURL}/super`;
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  postSuperStart: async (task) => {
    const url = `${APIsocket.baseURL}/super/start?task=${task}`;
    try {
      const response = await axios.post(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  postSuperPause: async (task) => {
    const url = `${APIsocket.baseURL}/super/pause?task=${task}`;
    try {
      const response = await axios.post(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  postSuperStop: async (task) => {
    const url = `${APIsocket.baseURL}/super/stop?task=${task}`;
    try {
      const response = await axios.post(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },

  getRealTime: async () => {
    const url = `${APIsocket.baseURL}/realTime`;
    try {
      const response = await axios.get(url);
      return response.data;
    } catch (error) {
      throw error;
    }
  },
};

export default APIsocket;
