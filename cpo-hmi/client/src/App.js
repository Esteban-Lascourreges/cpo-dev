import React, { useEffect } from "react";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { createTheme } from "@mui/material/styles";
import { useMemo } from "react";
import { useSelector } from "react-redux";
import { BrowserRouter, Navigate, Routes, Route } from "react-router-dom";
import { themeSettings } from "theme";
import APIsocket from "APIsocket";

import Layout from "scenes/layout";

import Login from "scenes/login";
import Dashboard from "scenes/dashboard";
import Power from "scenes/power";
import SeaState from "scenes/power/seastate";
import LoseAndEfficiency from "scenes/power/loseandefficiency";
import PowerOutput from "scenes/power/poweroutput";
import GeneralBehavior from "scenes/power/generalbehavior";
import Availability from "scenes/power/availability";
import Modules from "scenes/modules";
import Overview from "scenes/modules/overview";
import History from "scenes/history";
import Signals from "scenes/history/signals";
import Events from "scenes/history/events";
import Report from "scenes/report";
import Notifications from "scenes/notifs";
import SystemInfo from "scenes/sysinfo";
import Settings from "scenes/settings";

/* Export WS responses/general settings */
export const sessionToken = generateSessionToken();


function generateSessionToken() {
  const sessionToken = sessionStorage.getItem("sessionToken");
  if (sessionToken) {
    return sessionToken;
  }
  const timestamp = new Date().getTime(); // Get the current timestamp
  const random = Math.random(); // Generate a random number
  // Combine the timestamp and random number to create a unique token
  sessionStorage.setItem("sessionToken", `${timestamp}-${random}`);
  return `${timestamp}-${random}`;
}
function App() {
  const mode = useSelector((state) => state.global.mode);
  const theme = useMemo(() => createTheme(themeSettings(mode)), [mode]);

  useEffect(() => {
    const handleBeforeUnload = (event) => {
      APIsocket.deleteSession(sessionToken);
    };
  
    window.addEventListener('beforeunload', handleBeforeUnload);
  
    return () => {
      // Remove the event listener when the component unmounts
      window.removeEventListener('beforeunload', handleBeforeUnload);
    };
  }, []);

  return (
    <div className="app">
      <BrowserRouter>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route element={<Layout />}>
              <Route path="/" element={<Navigate to="/login" replace />} />
              <Route path="/dashboard" element={<Dashboard />} />
              <Route path="/power" element={<Power />} />
              <Route path="/power/availability" element={<Availability />} />
              <Route
                path="/power/general_behavior"
                element={<GeneralBehavior />}
              />
              <Route
                path="/power/lose_and_efficiency"
                element={<LoseAndEfficiency />}
              />
              <Route path="/power/power_output" element={<PowerOutput />} />
              <Route path="/power/sea_state" element={<SeaState />} />
              <Route path="/modules" element={<Modules />} />
              <Route path="/modules/overview" element={<Overview />} />
              <Route path="/history" element={<History />} />
              <Route path="/history/signals" element={<Signals />} />
              <Route path="/history/events" element={<Events />} />
              <Route path="/report" element={<Report />} />
              <Route path="/notifs" element={<Notifications />} />
              <Route path="/sysinfo" element={<SystemInfo />} />
              <Route path="/settings" element={<Settings />} />
            </Route>
          </Routes>
        </ThemeProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
