export const DataList = [
  {
    title: "The Data Tree Needs/Is Being Generated",
    value: "VALUE1",
    selectable: false,
  },
  {
    title: "Please wait for the data to load",
    value: "VALUE2",
    selectable: false,
  },
];
