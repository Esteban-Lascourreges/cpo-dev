import React, { useState, useEffect } from "react";
import {
  useTheme,
  Typography,
  Box,
  Table,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TableSortLabel,
  Paper,
} from "@mui/material"; // Import Material-UI components
import FilterListOutlinedIcon from '@mui/icons-material/FilterListOutlined';
import APIsocket from "../APIsocket"; // Import the APIsocket module

function groupEventsById(events) {
  const groupedEvents = {};

  events.forEach((event) => {
    const { id } = event;
    if (!groupedEvents[id]) {
      groupedEvents[id] = [];
    }
    groupedEvents[id].push(event);
  });

  return Object.values(groupedEvents);
}

function EventTable({ initialData }) {
  useTheme();

  const [data, setData] = useState(initialData);

  const [cpcOrderBy, setCpcOrderBy] = useState("");
  const [cpcOrder, setCpcOrder] = useState("asc");

  const [dpcOrderBy, setDpcOrderBy] = useState("");
  const [dpcOrder, setDpcOrder] = useState("asc");

  // Helper function to compare dates
  const compareDates = (dateA, dateB, order) => {
    const comparison = new Date(dateA).getTime() - new Date(dateB).getTime();
    return order === "asc" ? comparison : -comparison;
  };
  // Helper function to compare severities
  const compareSeverities = (groupA, groupB, order) => {
    const severityA = Array.from(
      new Set(groupA.severity)
    );
    const severityB = Array.from(
      new Set(groupB.severity)
    );
    const joinedSeverityA = severityA.join(", ");
    const joinedSeverityB = severityB.join(", ");
    return order === "asc"
      ? joinedSeverityA.localeCompare(joinedSeverityB)
      : joinedSeverityB.localeCompare(joinedSeverityA);
  };

  //const groupedDataCpc = groupEventsById(data.CPC);
  const sortedDataCpc = [...data.CPC].sort((a, b) => {
    if (cpcOrderBy === "id") {
      if (cpcOrder === "asc") {
        return a[cpcOrderBy] > b[cpcOrderBy] ? 1 : -1;
      } else {
        return b[cpcOrderBy] > a[cpcOrderBy] ? 1 : -1;
      }
    } else if (cpcOrderBy === "start") {
      return compareDates(a.start, b.start, cpcOrder);
    } else if (cpcOrderBy === "end") {
      return compareDates(a.end, b.end, cpcOrder);
    } else if (cpcOrderBy === "severity") {
      return compareSeverities(a, b, cpcOrder);
    }
    return 0;
  });
  //const groupedDataDpc = groupEventsById(data.DPC);
  const sortedDataDpc = [...data.DPC].sort((a, b) => {
    if (dpcOrderBy === "id") {
      if (dpcOrder === "asc") {
        return a[dpcOrderBy] > b[dpcOrderBy] ? 1 : -1;
      } else {
        return b[dpcOrderBy] > a[dpcOrderBy] ? 1 : -1;
      }
    } else if (dpcOrderBy === "start") {
      return compareDates(a.start, b.start, dpcOrder);
    } else if (dpcOrderBy === "end") {
      return compareDates(a.end, b.end, dpcOrder);
    } else if (dpcOrderBy === "severity") {
      return compareSeverities(a, b, dpcOrder);
    }
    return 0;
  });

  const handleSortCpc = (property) => {
    const isAscending = cpcOrderBy === property && cpcOrder === "asc";
    setCpcOrderBy(property);
    setCpcOrder(isAscending ? "desc" : "asc");
  };
  const handleSortDpc = (property) => {
    const isAscending = dpcOrderBy === property && dpcOrder === "asc";
    setDpcOrderBy(property);
    setDpcOrder(isAscending ? "desc" : "asc");
  };

  // Function to fetch report data
  const fetchEventData = async () => {
    try {
      const eventData = await APIsocket.getEventsData();
      setData(eventData);
    } catch (error) {
      console.error("Error fetching event data:", error);
    }
  };

  // Use the fetchReportData function on component mount
  useEffect(() => {
    if (!initialData) {
      fetchEventData(); // Initial fetch only if no initialData provided
    } else if (initialData) {
      setData(initialData); // Set the initial data if provided
    }

    const intervalId = setInterval(() => {
      fetchEventData(); // Fetch report data every 30 seconds
    }, 30000); // 30,000 milliseconds = 30 seconds

    // Clear the interval on component unmount
    return () => {
      clearInterval(intervalId);
    };
  }, [initialData]);

  return (
    <Box /* Active */
      display="flex"
      width="100%"
      flexDirection="row"
      gap="1rem" /* C4 */
    >
      <Box width="50%">
        <Typography variant="h6">CPC Events</Typography>
        <Box>
          <TableContainer
            component={Paper}
            sx={{ width: "100%", maxHeight: "35rem" }}
          >
            <Table size="small" stickyHeader>
              <TableHead>
                <TableRow>
                  <TableCell>
                    <TableSortLabel
                      active={cpcOrderBy === "id"}
                      direction={cpcOrderBy === "id" ? cpcOrder : "asc"}
                      onClick={() => handleSortCpc("id")}
                      IconComponent={FilterListOutlinedIcon}
                    >
                      ID
                    </TableSortLabel>
                  </TableCell>
                  <TableCell>
                    <TableSortLabel
                      active={cpcOrderBy === "start"}
                      direction={cpcOrderBy === "start" ? cpcOrder : "asc"}
                      onClick={() => handleSortCpc("start")}
                      IconComponent={FilterListOutlinedIcon}
                    >
                      Start Date
                    </TableSortLabel>
                  </TableCell>
                  <TableCell>
                    <TableSortLabel
                      active={cpcOrderBy === "end"}
                      direction={cpcOrderBy === "end" ? cpcOrder : "asc"}
                      onClick={() => handleSortCpc("end")}
                      IconComponent={FilterListOutlinedIcon}
                    >
                      End Date
                    </TableSortLabel>
                  </TableCell>
                  <TableCell>
                    <TableSortLabel
                      active={cpcOrderBy === "severity"}
                      direction={cpcOrderBy === "severity" ? cpcOrder : "asc"}
                      onClick={() => handleSortCpc("severity")}
                      IconComponent={FilterListOutlinedIcon}
                    >
                      Severities
                    </TableSortLabel>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {sortedDataCpc.map((event, index) => (
                  <TableRow key={`cpc-row-${index}`}>
                    <TableCell>
                      {event.id}
                    </TableCell>
                    <TableCell>
                      {event.start}
                    </TableCell>
                    <TableCell>
                      {event.end}
                    </TableCell>
                    <TableCell>
                      {Array.from(
                        new Set(event.severity)
                      ).join(", ")}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      </Box>
      <Box width="50%">
        <Typography variant="h6">DPC Events</Typography>
        <Box>
          <TableContainer
            component={Paper}
            sx={{ width: "100%", maxHeight: "35rem" }}
          >
            <Table size="small" stickyHeader>
              <TableHead>
                <TableRow>
                  <TableCell>
                    <TableSortLabel
                      active={dpcOrderBy === "id"}
                      direction={dpcOrderBy === "id" ? dpcOrder : "asc"}
                      onClick={() => handleSortDpc("id")}
                      IconComponent={FilterListOutlinedIcon}
                    >
                      ID
                    </TableSortLabel>
                  </TableCell>
                  <TableCell>
                    <TableSortLabel
                      active={dpcOrderBy === "start"}
                      direction={dpcOrderBy === "start" ? dpcOrder : "asc"}
                      onClick={() => handleSortDpc("start")}
                      IconComponent={FilterListOutlinedIcon}
                    >
                      Start Date
                    </TableSortLabel>
                  </TableCell>
                  <TableCell>
                    <TableSortLabel
                      active={dpcOrderBy === "end"}
                      direction={dpcOrderBy === "end" ? dpcOrder : "asc"}
                      onClick={() => handleSortDpc("end")}
                      IconComponent={FilterListOutlinedIcon}
                    >
                      End Date
                    </TableSortLabel>
                  </TableCell>
                  <TableCell>
                    <TableSortLabel
                      active={dpcOrderBy === "severity"}
                      direction={dpcOrderBy === "severity" ? dpcOrder : "asc"}
                      onClick={() => handleSortDpc("severity")}
                      IconComponent={FilterListOutlinedIcon}
                    >
                      Severities
                    </TableSortLabel>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {sortedDataDpc.map((event, index) => (
                  <TableRow key={`dpc-row-${index}`}>
                    <TableCell>
                      {event.id}
                    </TableCell>
                    <TableCell>
                      {event.start}
                    </TableCell>
                    <TableCell>
                      {event.end}
                    </TableCell>
                    <TableCell>
                      {Array.from(
                        new Set(event.severity)
                      ).join(", ")}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      </Box>
    </Box>
  );
}

export default EventTable;
