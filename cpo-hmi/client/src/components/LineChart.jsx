import { useTheme, Box } from "@mui/material";
import Plot from "react-plotly.js";

const LineChart = ({ DataSet, X_SignalName }) => {
  const theme = useTheme();
  const colors = theme.palette;
  const dataPlot = Object.keys(DataSet)
    .filter((signalKey) => signalKey !== X_SignalName) // Exclude "X Axis" signal
    .filter((signalKey) => signalKey !== "time") // Exclude "time" signal
    .map((signalKey) => ({
      x: DataSet[X_SignalName],
      y: DataSet[signalKey],
      type: "scatter",
      mode: "lines",
      name: signalKey,
    }));

  return (
    <Box display="flex" width="100%" height="100%">
      <Plot
        data={dataPlot}
        layout={{
          showlegend: true,
          legend: {
            title: { text: "Signals" },
            bgcolor: colors.background.default,
          },

          paper_bgcolor: colors.background.default,
          plot_bgcolor:
            theme.palette.mode === "dark"
              ? colors.primary[700]
              : colors.grey[100],

          font: { color: colors.text.primary },
          hovermode: "x unified",
          xaxis: {
            title: "Time (UTC+00:00)",
            color: colors.text.primary,
            gridcolor: colors.primary[200],
          },
          yaxis: {
            title: "Value",
            color: colors.text.primary,
            gridcolor: colors.primary[300],
          },
          responsive: true,
        }}
        useResizeHandler={true}
        style={{ display: "flex", width: "100%", height: "100%" }}
      />
    </Box>
  );
};

export default LineChart;
