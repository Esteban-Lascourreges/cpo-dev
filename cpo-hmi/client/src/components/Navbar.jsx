import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  LightModeOutlined,
  DarkModeOutlined,
  Menu as MenuIcon,
  SettingsOutlined,
  LogoutOutlined,
  HomeOutlined,
} from "@mui/icons-material";
import FlexBetween from "components/FlexBetween";
import { useDispatch } from "react-redux";
import { setMode } from "state";
import {
  AppBar,
  IconButton,
  Toolbar,
  useTheme,
  Divider,
} from "@mui/material";

import APIsocket from "APIsocket";
import { sessionToken } from "App";
import { loggedIn } from "scenes/layout";

const Navbar = ({ isSidebarOpen, setIsSidebarOpen }) => {
  const dispatch = useDispatch();
  const theme = useTheme();
  const navigate = useNavigate();
  const [ping, setPing] = useState(0);

  const handleLogout = () => {
    // Clear the token and user data from localStorage
    localStorage.removeItem("Credentials");
    try {
        APIsocket.deleteSession(sessionToken);
    } catch (error) {
        console.log(error);
    }
    // Redirect to the login page
    navigate("/login");
  };

  const fetchPingData = async () => {
    try {
      const ip = '10.39.100.10'; // Set the IP address
      const response = await APIsocket.getPingInfo(ip);
      setPing(response.averagePingTime);
    } catch (error) {
      console.error('Error fetching ping data:', error);
    }
  };

  useEffect(() => {
    fetchPingData(); // Initial fetch

    const intervalId = setInterval(() => {
      fetchPingData(); // Fetch ping data every 5 seconds
    }, 5000);

    // Clear the interval on component unmount
    return () => {
      clearInterval(intervalId);
    };
  }, []);

  return (
    <AppBar
      sx={{
        position: "static",
        backgroundColor: "none",
        boxShadow: "none",
      }}
    >
      <Toolbar sx={{ justifyContent: "space-between" }}>
        {/* LEFT SIDE */}
        <FlexBetween gap="0.5rem">
          <IconButton onClick={() => setIsSidebarOpen(!isSidebarOpen)}>
            <MenuIcon />
          </IconButton>
          <Divider orientation="vertical" variant="middle" flexItem />
          <div>Ping LPC0: {ping}ms</div>
          <Divider orientation="vertical" variant="middle" flexItem />
        </FlexBetween>
        {/* RIGHT SIDE */}
        <FlexBetween gap="0.5rem">
          <Divider orientation="vertical" variant="middle" flexItem />
          <div>
            {loggedIn.username.replace("@corpowerocean.com","")} ({loggedIn.role})
          </div>
          <Divider orientation="vertical" variant="middle" flexItem />
          <IconButton onClick={() => dispatch(setMode())}>
            {theme.palette.mode === "dark" ? (
              <DarkModeOutlined sx={{ fontSize: "25px" }} />
            ) : (
              <LightModeOutlined sx={{ fontSize: "25px" }} />
            )}
          </IconButton>
          <IconButton onClick={() => navigate("/dashboard")}>
            <HomeOutlined sx={{ fontSize: "25px" }} />
          </IconButton>
          <IconButton onClick={() => navigate("/settings")}>
            <SettingsOutlined sx={{ fontSize: "25px" }} />
          </IconButton>
          <IconButton
            onClick={() => {
              handleLogout();
            }}
          >
            <LogoutOutlined sx={{ fontSize: "25px" }} />
          </IconButton>
        </FlexBetween>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
