import { useTheme, Box } from "@mui/material";
import { useEffect, useState } from "react";

import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
import { DatePicker, ConfigProvider } from "antd";

dayjs.extend(customParseFormat);
var utc = require('dayjs/plugin/utc')
dayjs.extend(utc)

const { RangePicker } = DatePicker;

const RangeDatePicker = ({ ifChange, updateDates }) => {
  const MuiTheme = useTheme();
  const colors = MuiTheme.palette;

  const [values, setValues] = useState(['','']);

  const onChange = (dates, dateStrings) => {
    if (dates !== null) {
        if (dates[0].isBefore(dates[1])) {
            setValues(dates);
        } else {
            setValues([dates[1], dates[0]]);
        }
    }

    if (ifChange) {
      ifChange(dateStrings); // Call the provided inChange callback
    }
  };

  useEffect(() => {
    var d1 ='', d2 = '';
    if (updateDates.length !== 0) {
      if (updateDates[0]!=='') {
        d1 = dayjs(updateDates[0]);
      }
      if (updateDates[1]!=='') {
        d2 = dayjs(updateDates[1]);
      }
      setValues([d1, d2])
    }
    }, [updateDates]);
  const disabledDate = (current) => {
    // Can not select days after today and today
    return current && current > dayjs.utc().endOf("day");
  };
  return (
    <Box display="flex" gap="1rem" alignItems="center">
      <ConfigProvider
        theme={{
          token: {
            colorTextBase: colors.text.primary,
            colorPrimaryBg: colors.secondary[500],
            colorBgBase:
              colors.mode === "dark" ? colors.primary[700] : colors.grey[100],
            colorPrimary: colors.secondary[700],
            colorBorder: colors.secondary[700],
          },
        }}
      >
        <RangePicker
          onChange={onChange}
          //status={status}
          disabledDate={disabledDate}
          value={values}
          allowClear={true}
          allowEmpty={[true, true]}
          presets={[
            {
              label: "Last 1 mins",
              value: () => [dayjs.utc().add(-1, "m"), dayjs.utc()], // 5.8.0+ support function
            },
            {
              label: "Last 5 mins",
              value: () => [dayjs.utc().add(-5, "m"), dayjs.utc()], // 5.8.0+ support function
            },
            {
              label: "Last 15 mins",
              value: () => [dayjs.utc().add(-15, "m"), dayjs.utc()], // 5.8.0+ support function
            },
            {
              label: "Last 30 mins",
              value: () => [dayjs.utc().add(-30, "m"), dayjs.utc()], // 5.8.0+ support function
            },
            {
              label: "Last Hour",
              value: () => [dayjs.utc().add(-1, "h"), dayjs.utc()], // 5.8.0+ support function
            },
            {
              label: "Last 6 Hours",
              value: () => [dayjs.utc().add(-6, "h"), dayjs.utc()], // 5.8.0+ support function
            },
            {
              label: "Last 12 Hours",
              value: () => [dayjs.utc().add(-12, "h"), dayjs.utc()], // 5.8.0+ support function
            },
            {
              label: "Last 24 Hours",
              value: () => [dayjs.utc().add(-1, "d"), dayjs.utc()], // 5.8.0+ support function
            },
          ]}
          showTime={{
            hideDisabledOptions: true,
          }}
          format="YYYY-MM-DD HH:mm:ss"
        />
      </ConfigProvider>
    </Box>
  );
};

export default RangeDatePicker;
