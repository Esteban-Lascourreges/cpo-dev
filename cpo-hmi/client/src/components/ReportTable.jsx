import React, { useState, useEffect } from "react";
import {
  useTheme,
  Box,
  Table,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Paper,
} from "@mui/material"; // Import Material-UI components
import APIsocket from "../APIsocket"; // Import the APIsocket module

function ReportTable({ initialData }) {
  useTheme();

  const [data, setData] = useState(initialData);

  // Function to fetch report data
  const fetchReportData = async () => {
    try {
      const reportData = await APIsocket.getReport();
      setData(reportData);
    } catch (error) {
      console.error("Error fetching report data:", error);
    }
  };

  // Use the fetchReportData function on component mount
  useEffect(() => {
    if (!initialData) {
      fetchReportData(); // Initial fetch only if no initialData provided
    } else if (initialData) {
      setData(initialData); // Set the initial data if provided
    }

    const intervalId = setInterval(() => {
      fetchReportData(); // Fetch report data every 30 seconds
    }, 30000); // 30,000 milliseconds = 30 seconds

    // Clear the interval on component unmount
    return () => {
      clearInterval(intervalId);
    };
  }, [initialData]);

  return (
    <Box>
      {data && (
        <TableContainer
          component={Paper}
          sx={{
            maxHeight: "400px",
          }}
        >
          <Table size="small" stickyHeader>
            <TableHead>
              <TableRow>
                {data.signals.map((signal, index) => (
                  <TableCell key={index}>{signal}</TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {data.values.map((valueRow, rowIndex) => (
                <TableRow key={rowIndex}>
                  {valueRow.map((value, columnIndex) => (
                    <TableCell key={columnIndex}>{value}</TableCell>
                  ))}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    </Box>
  );
}

export default ReportTable;
