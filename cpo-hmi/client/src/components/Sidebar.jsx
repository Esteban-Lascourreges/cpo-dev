import React from "react";
import {
  Box,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
  useTheme,
} from "@mui/material";
import {
  ChevronLeft,
  ChevronRightOutlined,
  HomeOutlined /* Home/Dashboard */,
  PowerOutlined /* Power */,
  MiscellaneousServicesOutlined /* Multi Cog (Modules) */,
  HistoryOutlined /* History */,
  SummarizeOutlined /* Report */,
  CampaignOutlined /* Alerts */,
  InfoOutlined /* Info */,
  TuneOutlined /* Settings */,
} from "@mui/icons-material";
import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import FlexBetween from "components/FlexBetween";
import CPO_Logo_Navy_Blue_RGB from "assets/CPO_Logo_Navy_Blue_RGB.png";
import CPO_Logo_White_RGB from "assets/CPO_Logo_White_RGB.png";

const navItems = [
  {
    /* Home */
    text: "Dashboard",
    icon: <HomeOutlined />,
    selectable: true,
    pos: 0,
    path: "dashboard",
  },
  //{
  //  /* Power */
  //  text: "Power",
  //  icon: <PowerOutlined />,
  //  selectable: false,
  //  pos: 0,
  //  path: "power",
  //},
  //{
  //  /* Power - Sea State */
  //  text: "Sea State",
  //  icon: null,
  //  selectable: true,
  //  pos: 1,
  //  path: "power/sea_state",
  //},
  //{
  //  /* Power - Loses & Efficiency */
  //  text: "Loses & Efficiency",
  //  icon: null,
  //  selectable: true,
  //  pos: 1,
  //  path: "power/lose_and_efficiency",
  //},
  //{
  //  /* Power - Power Output */
  //  text: "Power Output",
  //  icon: null,
  //  selectable: true,
  //  pos: 1,
  //  path: "power/power_output",
  //},
  //{
  //  /* Power - General behavior */
  //  text: "General Behavior",
  //  icon: null,
  //  selectable: true,
  //  pos: 1,
  //  path: "power/general_behavior",
  //},
  //{
  //  /* Power - Availability */
  //  text: "Availability",
  //  icon: null,
  //  selectable: true,
  //  pos: 1,
  //  path: "power/availability",
  //},
  //{
  //  /* Modules */
  //  text: "Modules",
  //  icon: <MiscellaneousServicesOutlined />,
  //  selectable: false,
  //  pos: 0,
  //  path: "modules",
  //},
  //{
  //  /* Modules - Overview */
  //  text: "Overview",
  //  icon: null,
  //  selectable: true,
  //  pos: 1,
  //  path: "modules/overview",
  //},
  {
    /* History */
    text: "History",
    icon: <HistoryOutlined />,
    selectable: false,
    pos: 0,
    path: "history",
  },
  {
    /* History - Signals */
    text: "Signals",
    icon: null,
    selectable: true,
    pos: 1,
    path: "history/signals",
  },
  {
    /* History - Events */
    text: "Events",
    icon: null,
    selectable: true,
    pos: 1,
    path: "history/events",
  },
  {
    /* Reports maker */
    text: "Report Maker",
    icon: <SummarizeOutlined/>,
    selectable: true,
    pos: 0,
    path: "report",
  },
  //{
  //  /* Notif */
  //  text: "Notifications",
  //  icon: <CampaignOutlined />,
  //  selectable: true,
  //  pos: 0,
  //  path: "notifs",
  //},
  {
    /* System Info */
    text: "System Info",
    icon: <InfoOutlined />,
    selectable: true,
    pos: 0,
    path: "sysinfo",
  },
  {
    /* Settings */
    text: "Settings",
    icon: <TuneOutlined />,
    selectable: true,
    pos: 0,
    path: "settings",
  },
];

const Sidebar = ({
  drawerWidth,
  isSidebarOpen,
  setIsSidebarOpen,
  isNonMobile,
}) => {
  const { pathname } = useLocation();
  const [active, setActive] = useState("");
  const navigate = useNavigate();
  const theme = useTheme();
  const colors = theme.palette;

  useEffect(() => {
    setActive(pathname.substring(1));
  }, [pathname]);

  return (
    <Box component="nav">
      {isSidebarOpen && (
        <Drawer
          open={isSidebarOpen}
          onClose={() => setIsSidebarOpen(false)}
          variant="persistent"
          anchor="left"
          sx={{
            width: drawerWidth,
            "& .MuiDrawer-paper": {
              overflowX: "hidden",
              overflowY: "hidden",
              "&:hover": {
                overflowY: "auto",
              },
              "&::-webkit-scrollbar": {
                display: "none",
              },
              color: theme.palette.text.secondary,
              backgroundColor: theme.palette.background.alt,
              boxSizing: "border-box",
              borderWidth: isNonMobile ? 0 : "2px",
              width: drawerWidth,
            },
          }}
        >
          <Box width="100%" display="flex" flexDirection="column">
            <Box
              display="flex"
              height="4rem"
              alignItems="center"
              justifyContent="center"
            >
              <FlexBetween color={theme.palette.secondary.main}>
                <Box>
                    <img src={colors.mode==="dark" ? CPO_Logo_White_RGB : CPO_Logo_Navy_Blue_RGB} height="40rem" alt="Logo"/>
                </Box>
                {!isNonMobile && (
                  <IconButton onClick={() => setIsSidebarOpen(!isSidebarOpen)}>
                    <ChevronLeft />
                  </IconButton>
                )}
              </FlexBetween>
            </Box>
            <List>
              {navItems.map(({ text, icon, selectable, pos, path }) => {
                if (!selectable) {
                  if (!icon) {
                    return (
                      <ListItem
                        key={path}
                        sx={{ m: "0.5rem 0 0.5rem 0rem", ml: `${pos}rem` }}
                      >
                        <Typography>{text}</Typography>
                      </ListItem>
                    );
                  } else {
                    return (
                      <ListItem
                        key={path}
                        sx={{ m: "0.5rem 0 0.5rem 0rem", ml: `${pos}rem` }}
                      >
                        <ListItemIcon
                          sx={{
                            color: theme.palette.primary[200],
                          }}
                        >
                          {icon}
                        </ListItemIcon>
                        <Typography>{text}</Typography>
                      </ListItem>
                    );
                  }
                } else {
                  if (!icon) {
                    return (
                      <ListItem key={path} disablePadding>
                        <ListItemButton
                          onClick={() => {
                            setActive(path);
                            navigate(path);
                          }}
                          sx={{
                            ml: `${pos}rem`,
                            backgroundColor:
                              active === path
                                ? theme.palette.secondary[300]
                                : "transparent",
                            color:
                              active === path
                                ? theme.palette.primary[600]
                                : theme.palette.text.secondary[100],
                          }}
                        >
                          <ListItemText primary={text} />
                          {active === path && (
                            <ChevronRightOutlined sx={{ ml: "auto" }} />
                          )}
                        </ListItemButton>
                      </ListItem>
                    );
                  } else {
                    return (
                      <ListItem key={path} disablePadding>
                        <ListItemButton
                          onClick={() => {
                            setActive(path);
                            navigate(path);
                          }}
                          sx={{
                            ml: `${pos}rem`,
                            backgroundColor:
                              active === path
                                ? theme.palette.secondary[300]
                                : "transparent",
                            color:
                              active === path
                                ? theme.palette.primary[600]
                                : theme.palette.text.secondary[100],
                          }}
                        >
                          <ListItemIcon
                            sx={{
                              color:
                                active === path
                                  ? theme.palette.primary[600]
                                  : theme.palette.text.secondary[200],
                            }}
                          >
                            {icon}
                          </ListItemIcon>
                          <ListItemText primary={text} />
                          {active === path && (
                            <ChevronRightOutlined sx={{ ml: "auto" }} />
                          )}
                        </ListItemButton>
                      </ListItem>
                    );
                  }
                }
              })}
            </List>
          </Box>
        </Drawer>
      )}
    </Box>
  );
};

export default Sidebar;
