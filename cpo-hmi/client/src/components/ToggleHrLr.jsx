import React from "react";
import { useTheme, Box } from "@mui/material";

import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";

const ToggleHrLr = ({ ifChange }) => {
  useTheme();

  const [alignment, setAlignment] = React.useState("web");

  const onChange = (event, newAlignment) => {
    setAlignment(newAlignment);
    if (ifChange) {
      ifChange(newAlignment); // Call the provided inChange callback
    }
  };

  return (
    <Box display="flex" gap="1rem" alignItems="center">
      <ToggleButtonGroup
        size="small"
        value={alignment}
        exclusive
        onChange={onChange}
        aria-label="Rate of Data"
      >
        <ToggleButton value="LR">Low Rate</ToggleButton>
        <ToggleButton value="HR">High Rate</ToggleButton>
      </ToggleButtonGroup>
    </Box>
  );
};

export default ToggleHrLr;
