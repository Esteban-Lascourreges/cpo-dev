import React, { useState, useEffect } from "react";
import { useTheme, Box } from "@mui/material";

import { TreeSelect, ConfigProvider } from "antd";

import { DataList } from "assets/DataList_EMPTY.js";
import APIsocket from "APIsocket";

const TreeSelector = ({ rate, Reset, ifChange, updateSelected, disabled}) => {
  const MuiTheme = useTheme();
  const colors = MuiTheme.palette;
  const [hasLoaded, setHasLoaded] = useState(false);

  const [value, setValue] = useState();

  const [HR_Data_Tree, setHR_Data_Tree] = useState(DataList);
  const [LR_Data_Tree, setLR_Data_Tree] = useState(DataList);

  const getDataTree = async () => {
    const HrTree = await APIsocket.getDataTree("HR");
    setHR_Data_Tree(DataList);
    const LrTree = await APIsocket.getDataTree("LR");
    setLR_Data_Tree(LrTree);
  };

  const onChange = (newValue) => {
    setValue(newValue);
    if (ifChange) {
      ifChange(newValue); // Call the provided inChange callback
    }
  };

  useEffect(() => {
    if (!hasLoaded) {
      setHasLoaded(true);
      getDataTree();
    }
    setValue(updateSelected)
  }, [Reset, updateSelected, hasLoaded]);

  return (
    <Box display="flex" gap="1rem" alignItems="center">
      <ConfigProvider
        theme={{
          token: {
            colorTextBase: colors.text.primary,
            colorPrimaryBg: colors.secondary[500],
            colorBgBase:
              colors.mode === "dark" ? colors.primary[700] : colors.grey[100],
            colorPrimary: colors.secondary[700],
            colorBorder: colors.secondary[700],
          },
        }}
      >
        <TreeSelect
          style={{
            minWidth: "25rem",
            maxWidth: "80%",
            color: colors.text.primary,
          }}
          treeData={rate === "LR" ? LR_Data_Tree : HR_Data_Tree}
          value={value}
          dropdownStyle={{
            maxHeight: 400,
            overflow: "auto",
          }}
          placeholder="Select a Signal"
          allowClear
          multiple
          //status={status}
          treeLine
          onChange={onChange}
          autoClearSearchValue={false}
          disabled={disabled}
        />
      </ConfigProvider>
    </Box>
  );
};

export default TreeSelector;
