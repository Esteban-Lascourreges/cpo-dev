import React, { useState, useEffect } from "react";
import {
  Table,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Paper,
} from "@mui/material";
import APIsocket from "APIsocket"; // Import your APIsocket as needed

function UserTable() {
  const [userData, setUserData] = useState({});

  // Function to fetch user data and update state
  const fetchUserData = async () => {
    const data = await APIsocket.getUsers();
    setUserData(data);
  };

  // Initial data fetch on component mount
  useEffect(() => {
    fetchUserData();

    // Set up an interval to fetch data every 30 minutes
    const interval = setInterval(() => {
      fetchUserData();
    }, 1800000); // 30 minutes in milliseconds (30 * 60 * 1000)

    return () => clearInterval(interval); // Clean up the interval on unmount
  }, []);

  return (
    <TableContainer component={Paper}>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Username</TableCell>
            <TableCell>Role</TableCell>
            <TableCell>Session ID</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {Object.keys(userData).map((key) => (
            <TableRow key={key}>
              <TableCell>{userData[key].username}</TableCell>
              <TableCell>{userData[key].role}</TableCell>
              <TableCell>{userData[key].sessionID}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default UserTable;
