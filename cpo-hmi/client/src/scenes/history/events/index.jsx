import React, { useState, useEffect } from "react";
import {
  Button,
  Box,
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TableSortLabel,
  Typography,
  useTheme,
} from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

import Header from "components/Header";
import event_info from "assets/event_info.json";
import APIsocket from "APIsocket";
import EventTable from "components/EventTables";

const Events = () => {
  useTheme();
  const [hasLoaded, setHasLoaded] = useState(false);
  const [initialData, setInitialData] = useState({ CPC: [], DPC: [] });

  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("");
  const [expandedRow, setExpandedRow] = useState(null);
  const [tableCollapsed, setTableCollapsed] = useState(false);

  const sortedDataInfo = Object.values(event_info.data).sort((a, b) => {
    const aValue = a[orderBy];
    const bValue = b[orderBy];
    // Handle null values by placing them at the end
    if (aValue === null && bValue !== null) {
      return 1;
    } else if (aValue !== null && bValue === null) {
      return -1;
    }
    // Handle string sorting
    const compareValue = order === "asc" ? -1 : 1;
    if (aValue < bValue) return compareValue;
    if (aValue > bValue) return -compareValue;
    return 0;
  });
  const columnsInfo = [
    { id: "id", label: "ID" },
    { id: "name", label: "Name" },
  ];
  const handleRequestEvtInfoSort = (property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };
  const handleRowClick = (id) => {
    setExpandedRow(expandedRow === id ? null : id);
  };
  const toggleTableCollapse = () => {
    setTableCollapsed(!tableCollapsed);
  };
  const fetchEventData = async () => {
    try {
      const eventData = await APIsocket.getEventsData();
      setInitialData(eventData);
    } catch (error) {
      console.error("Error fetching event data:", error);
    }
  };
  useEffect(() => {
    if (!hasLoaded) {
      setHasLoaded(true);
      fetchEventData();
    }
  }, [hasLoaded]);

  return (
    <Box m="0.5rem 0 1rem 2rem" /* Page */>
      <Header
        title="Event"
        subtitle="See the different Events happening in the system."
      />
      <Box
        mt="0.5rem"
        display="flex"
        flexDirection="column"
        gap="1rem" /* Content */
      >
        <Typography variant="h5">Active Events</Typography>
        <EventTable initialData={initialData} />
        <Box /* Definition */ display="flex" width="90%" flexDirection="column">
          <Box display="flex" alignItems="center" gap="0.5rem">
            <Typography variant="h5">Event definitions</Typography>
          </Box>

          <TableContainer
            component={Paper}
            sx={{ width: "100%", maxHeight: "25rem" }}
          >
            <Table size="small" stickyHeader>
              <TableHead>
                <TableRow>
                  {columnsInfo.map((column, index) => (
                    <TableCell key={`${column.id}_${index}`}>
                      {tableCollapsed ? (
                        <TableSortLabel disabled={true}>
                          {column.label}
                        </TableSortLabel>
                      ) : (
                        <TableSortLabel
                          active={orderBy === column.id}
                          direction={orderBy === column.id ? order : "asc"}
                          onClick={() => handleRequestEvtInfoSort(column.id)}
                        >
                          {column.label}
                        </TableSortLabel>
                      )}
                      {column.id === "name" && (
                        <Button
                          onClick={toggleTableCollapse}
                          variant="outlined"
                          color="secondary"
                          size="small"
                        >
                          {tableCollapsed ? "Expand Table" : "Collapse Table"}
                        </Button>
                      )}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              {!tableCollapsed && ( // Render the table only if it's not collapsed
                <TableBody key={orderBy}>
                  {sortedDataInfo.map((event, index) => (
                    <TableRow
                      key={event.id}
                      onClick={() => handleRowClick(event.id)}
                      style={{ cursor: "pointer" }}
                    >
                      {columnsInfo.map((column, index) => (
                        <TableCell key={`${column.id}_${index}`}>
                          {column.id === "id" && (
                            <Box display="flex" alignItems="center">
                              {event[column.id]}
                              <ExpandMoreIcon
                                style={{
                                  transform:
                                    expandedRow === event.id
                                      ? "rotate(180deg)"
                                      : "rotate(0deg)",
                                  transition: "transform 0.3s",
                                }}
                              />
                            </Box>
                          )}
                          {column.id === "name" && (
                            <Box>
                              {event[column.id]}
                              {expandedRow === event.id && ( // Render collapsible content only if row is expanded
                                <Box margin={1}>
                                  <p>Description: {event.description}</p>
                                  <p>Hardware Failure: {event.hwFailure}</p>
                                  <p>HMI: {event.hmi}</p>
                                </Box>
                              )}
                            </Box>
                          )}
                        </TableCell>
                      ))}
                    </TableRow>
                  ))}
                </TableBody>
              )}
            </Table>
          </TableContainer>
        </Box>
      </Box>
    </Box>
  );
};

export default Events;
