import React, { useEffect, useState } from "react";
import { Box, useTheme } from "@mui/material";
import {
  TimelineOutlined /* LineChart */,
  FileDownloadOutlined /* Save Data */,
  FileDownloadDoneOutlined /* Saved Data */,
} from "@mui/icons-material";
import Button from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";
import dayjs from "dayjs";

import Header from "components/Header";
import RangeDatePicker from "components/RangeDatePicker";
import ToggleHrLr from "components/ToggleHrLr";
import TreeSelector from "components/TreeSelector";
import LineChart from "components/LineChart";

import APIsocket from "APIsocket";

const Signals = () => {
  useTheme();

  const [Date1, setDate1] = useState("");
  const [Date2, setDate2] = useState("");
  const [Rate, setRate] = useState("");
  const [selected, setSelected] = useState([]);
  const [data, setData] = useState({});
  const [reset, setReset] = useState(0);
  const [isFetchLoading, setIsFetchLoading] = useState(false);

  const [isDlLoading, setIsDlLoading] = useState(false);
  const [isDownloaded, setIsDownloaded] = useState(false);
  const [isDlAble, setIsDlAble] = useState(false);

  const [requestable, setRequestable] = useState(false);

  useEffect(() => {
    const stat = Date1 && Date2 && Rate !== null && selected.length > 0;
    const isRequestable = stat ? true : false;
    setRequestable(isRequestable);

  }, [Date1, Date2, Rate, selected, requestable, setRequestable]);

  const convertJsonToCsv = (data) => {
    const headers = Object.keys(data); // Extract keys as headers
    const csvRows = [headers.join(",")]; // Add headers to CSV rows

    const numRows = data[headers[0]].length; // Get the number of rows

    // Loop through each row
    for (let i = 0; i < numRows; i++) {
      const rowValues = headers.map((header) => data[header][i]);
      csvRows.push(rowValues.join(","));
    }

    return csvRows.join("\n");
  };
  const convertToCsv = (DataToSave) => {
    if (DataToSave.time.length > 0) {
      const csv = convertJsonToCsv(DataToSave);

      // Create a Blob
      const blob = new Blob([csv], { type: "text/csv" });
      const url = URL.createObjectURL(blob);

      // Create a download link
      const a = document.createElement("a");
      a.href = url;
      const D1 = dayjs.utc(Date1).format("YYYY-MM-DD-T-HH-mm-ss");
      const D2 = dayjs.utc(Date2).format("YYYY-MM-DD-T-HH-mm-ss");
      a.download = "Data_-_" + D1 + "_-_" + D2 + ".csv";
      a.click();

      // Cleanup after the download is complete
      URL.revokeObjectURL(url);
      setTimeout(() => {
        // Perform your action here (e.g., show a message)
        setIsDownloaded(true);
        setIsDlLoading(false);
        console.log("Download complete!");
      }, 1000); // 1000 milliseconds = 1 second
    }
  };

  const handleTreeSelectorChange = (newValue) => {
    setSelected(newValue);
  };
  const handleDateRangeChange = (newValue) => {
    setDate1(newValue[0]);
    setDate2(newValue[1]);
  };
  const handleRateSelectorChange = (newValue) => {
    setRate(newValue);
    setReset(reset === 0 ? 1 : 0);
    setSelected([]);
  };

  /* Get/Save */
  const handleFetchClick = async () => {
    setIsFetchLoading(true);
    const Data = await APIsocket.getSignals(Rate, selected, Date1, Date2)
    setIsFetchLoading(false);
    setIsDlAble(true);
    setData(Data);
  };
  const handleSaveClick = () => {
    /* Saves data */
    setIsDlLoading(true);
    convertToCsv(data);
  };

  return (
    <Box m="0.5rem 0 1rem 2rem">
      <Header
        title="Signals"
        subtitle="Visualize Low or High Rate signals, with the ability to save them as CSV"
      />
      <Box /*Content*/>
        <Box /*Settings*/ display="flex" flexDirection="column" gap="0.5rem">
          <Box /* Select Data / Rate */
            display="inline-flex"
            gap="1rem"
            mt="1rem"
            alignItems="center"
          >
            <Box>
              <RangeDatePicker
                ifChange={handleDateRangeChange}
                updateDates={[Date1, Date2]}
              />
            </Box>
            <Box /*Toggle High/Low Rate*/ /*onClick={handleRateToggle}*/>
              <ToggleHrLr ifChange={handleRateSelectorChange} />
            </Box>
          </Box>
          <Box /*Tree Selector*/>
            <TreeSelector
              rate={Rate}
              Reset={reset}
              ifChange={handleTreeSelectorChange}
            />
          </Box>
          <Box
            /*Buttons*/
            display="flex"
            flexDirection="column"
            gap="0.5rem"
          >
            <Box display="inline-flex" gap="0.25rem" alignItems="center">
              <Button /*Fetch Data*/
                variant="contained"
                endIcon={
                  isFetchLoading ? (
                    <CircularProgress size={"1rem"} />
                  ) : (
                    <TimelineOutlined />
                  )
                }
                color={requestable ? "secondary" : "error"}
                disabled={
                  !requestable ||
                  isFetchLoading ||
                  (dayjs(Date2).diff(dayjs(Date1), "m") > 30 &&
                    Rate === "HR") ||
                  dayjs(Date1) > dayjs.utc()
                }
                onClick={handleFetchClick}
              >
                Display Data
              </Button>
            </Box>
            <Box display="inline-flex" gap="0.25rem" alignItems="center">
              <Button /*Save Data*/
                variant="contained"
                endIcon={
                  isDlLoading ? (
                    <CircularProgress size={"1rem"} />
                  ) : isDownloaded ? (
                    <FileDownloadDoneOutlined />
                  ) : (
                    <FileDownloadOutlined />
                  )
                }
                color={isDlAble ? "secondary" : "error"}
                disabled={!isDlAble}
                onClick={handleSaveClick}
              >
                Save Data
              </Button>
            </Box>
          </Box>
        </Box>
        <Box /*Display Data*/>
          <LineChart 
            DataSet={data} 
            X_SignalName="time"
          />
        </Box>
      </Box>
    </Box>
  );
};

export default Signals;
