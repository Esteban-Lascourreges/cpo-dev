import React, { useState, useEffect } from "react";
import { Box, useMediaQuery } from "@mui/material";
import { Outlet, useNavigate } from "react-router-dom";

import APIsocket from "APIsocket";
import { sessionToken } from "App";

import Navbar from "components/Navbar";
import Sidebar from "components/Sidebar";

import { DataList } from "assets/DataList_EMPTY.js";
export var LR_Data_Tree = DataList;
export var HR_Data_Tree = DataList;
export var loggedIn = { loggedIn: false, username: "", role: "", access: "" };

const Layout = () => {
  const isNonMobile = useMediaQuery("(min-width: 600px)");
  const [isSidebarOpen, setIsSidebarOpen] = useState(true);
  const navigate = useNavigate();
  const [hasLoaded, setHasLoaded] = useState(false);

  const genDataTree = async() => {
    HR_Data_Tree = DataList;//await APIsocket.getDataTree("HR");
    LR_Data_Tree = await APIsocket.getDataTree("LR");
    const DataTree = {
        LrTree: LR_Data_Tree,
        HrTree: HR_Data_Tree,
        };
    localStorage.setItem("DataTrees", JSON.stringify(DataTree));
  };
  
  const IsLoginCorrect = async (UserInfo) => {
    try {
        const Credentials = await APIsocket.login(UserInfo.username);
        loggedIn = {
            loggedIn: true,
            username: Credentials.username,
            role: Credentials.role,
            access: Credentials.access,
        };
        await APIsocket.createSession(
            Credentials.username,
            Credentials.role,
            sessionToken
          );
        return UserInfo.username === Credentials.username
    } catch (error) {
        console.log("Wrong credentials");
        console.log(error);
        localStorage.removeItem("Credentials");
        navigate("/login");
    }
  };

  useEffect(() => {
    if (!hasLoaded) {
      setHasLoaded(true);
      const token = localStorage.getItem("Credentials");
      if (token) {
        const UserInfo = JSON.parse(token);
        if (!IsLoginCorrect(UserInfo)) {
            localStorage.removeItem("Credentials");
            navigate("/login");
        }

        /* DATA Trees */
        genDataTree();

        /* "RealTime" Data */

      } else {
        navigate("/login");
      }
    }
  }, [hasLoaded]);
  return (
    <Box display={isNonMobile ? "flex" : "block"} width="100%" height="100%">
      <Sidebar
        isNonMobile={isNonMobile}
        drawerWidth="255px"
        isSidebarOpen={isSidebarOpen}
        setIsSidebarOpen={setIsSidebarOpen}
      />
      <Box flexGrow={1} width={isSidebarOpen ? window.innerWidth - 255 : window.innerWidth}>
        <Navbar
          isSidebarOpen={isSidebarOpen}
          setIsSidebarOpen={setIsSidebarOpen}
        />
        <Outlet/>
      </Box>
    </Box>
  );
};

export default Layout;
