import React, { useState, useEffect } from "react";
import {
  useTheme,
  Box,
  Button,
  TextField,
  Container,
  Typography,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import CPO_Logo_White_RGB from "assets/CPO_Logo_White_RGB.png";

import APIsocket from "APIsocket";

import { sessionToken } from "App";

const Login = () => {
  const MuiTheme = useTheme();
  const colors = MuiTheme.palette;
  const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [isValidEmail, setIsValidEmail] = useState(false);
  const [hasLoaded, setHasLoaded] = useState(false);

  const handleUsernameChange = (event) => {
    const newUsername = event.target.value;
    setUsername(newUsername);
    setIsValidEmail(validateEmail(newUsername));
  };

  const validateEmail = (email) => {
    // Simple email validation regex
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    // Check if the username is a valid email before allowing login
    if (isValidEmail) {
      // Perform your login logic here
      try {
        const Credentials = await APIsocket.login(username);
        const loggedIn = {
          loggedIn: true,
          username: Credentials.username,
          role: Credentials.role,
          access: Credentials.access,
        };
        localStorage.setItem("Credentials", JSON.stringify(loggedIn));
        await APIsocket.createSession(
          Credentials.username,
          Credentials.role,
          sessionToken
        );
        navigate("/dashboard");
      } catch (error) {
        console.log("Wrong credentials");
        setIsValidEmail(false);
        console.log(error);
      }
    }
  };

  useEffect(() => {
    if (!hasLoaded) {
      setHasLoaded(true);
      const token = localStorage.getItem("Credentials");
      if (token) {
        navigate("/dashboard");
      }
    }
  }, [hasLoaded]);

  return (
    <Box
      height="100%"
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <Container
        maxWidth="xs"
        sx={{
          display: "flex",
          flexDirection: "column",
          backgroundColor: colors.secondary[900],
          gap: "2rem",
          pt: "2rem",
          pb: "2rem",
          borderRadius: "1rem",
        }}
      >
        <img src={CPO_Logo_White_RGB} alt="CPO_Logo_White_RGB" p="1rem" />
        <Box>
          <Typography variant="h4" color="">
            Login
          </Typography>
          <form onSubmit={handleSubmit}>
            <TextField
              label="Username (Email)"
              variant="outlined"
              fullWidth
              margin="normal"
              name="username"
              value={username}
              onChange={handleUsernameChange}
              error={!isValidEmail}
              helperText={!isValidEmail ? "Invalid email address" : ""}
            />
            <Button
              variant="contained"
              color="primary"
              type="submit"
              disabled={!isValidEmail}
            >
              Login
            </Button>
          </form>
        </Box>
      </Container>
    </Box>
  );
};

export default Login;
