import React, { useEffect, useState, useRef } from "react";
import { Box, useTheme } from "@mui/material";
import dayjs from "dayjs";

import Slider from "@mui/material/Slider";
import Button from "@mui/material/Button";

import ButtonGroup from "@mui/material/ButtonGroup";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import Grow from "@mui/material/Grow";
import Paper from "@mui/material/Paper";
import Popper from "@mui/material/Popper";
import MenuItem from "@mui/material/MenuItem";
import MenuList from "@mui/material/MenuList";

import CircularProgress from "@mui/material/CircularProgress";
import RadioButtonCheckedOutlinedIcon from "@mui/icons-material/RadioButtonCheckedOutlined";
import RadioButtonUncheckedOutlinedIcon from "@mui/icons-material/RadioButtonUncheckedOutlined";

import {
  FileDownloadOutlined /* Save Data */,
  FileDownloadDoneOutlined /* Saved Data */,
} from "@mui/icons-material";

import TreeSelector from "components/TreeSelector";
import RangeDatePicker from "components/RangeDatePicker";
import Header from "components/Header";
import ReportTable from "components/ReportTable";

import { loggedIn } from "scenes/layout";
import APIsocket from "APIsocket";

const menuOptions = ["GENERATE", "RECORD"];

const Report = () => {
  useTheme();
  const [hasLoaded, setHasLoaded] = useState(false);

  const [menuButtonTxt, setMenuButtonTxt] = useState("");
  const [menuButtonIcon, setMenuButtonIcon] = useState(null);
  const [menuButtonTheme, setMenuButtonTheme] = useState("success");

  const [menuOpen, setMenuOpen] = useState(false);
  const anchorRefMenu = useRef(null);
  const [selectedIndex, setSelectedIndex] = useState(1);

  const [state, setState] = useState("toREC"); // toREC, REC, toGEN, GEN
  const [selected, setSelected] = useState([]);
  const [rate, setRate] = useState(60);
  const [rateRange, setRateRange] = useState(50);

  const [Date1, setDate1] = useState("");
  const [Date2, setDate2] = useState("");

  const [isDlLoading, setIsDlLoading] = useState(false);
  const [isDownloaded, setIsDownloaded] = useState(false);
  const [isDlAble, setIsDlAble] = useState(false);

  const [hasAccess, setHasAccess] = useState(false);

  const [RecordIcon, setRecordIcon] = useState(
    <RadioButtonCheckedOutlinedIcon />
  );
  const [isRecFirstIcon, setIsRecFirstIcon] = useState(true);

  const [initialData, setInitialData] = useState({ signals: [], values: [] });

  const marks = [
    { valuePct: 0, label: "1m", valueMn: 1 },
    { valuePct: 12.5, label: "5m", valueMn: 5 },
    { valuePct: 25, label: "15m", valueMn: 15 },
    { valuePct: 37.5, label: "30m", valueMn: 30 },
    { valuePct: 50, label: "1h", valueMn: 60 },
    { valuePct: 62.5, label: "3h", valueMn: 180 },
    { valuePct: 75, label: "6h", valueMn: 360 },
    { valuePct: 87.5, label: "12h", valueMn: 720 },
    { valuePct: 100, label: "24h", valueMn: 1440 },
  ];
  const tickMarks = marks.map((mark) => ({
    value: mark.valuePct,
    label: mark.label,
  }));

  const loadCurrentSettings = (state, selected, rate, startDate, endDate) => {
    if (startDate) {
      setDate1(startDate);
    }
    if (endDate) {
      setDate2(endDate);
    }
    setSelected(selected);
    setState(state);
    setRate(rate);

    const matchingMark = marks.find((mark) => mark.valueMn === rate);
    if (matchingMark) {
      setRate(matchingMark.valueMn);
      setRateRange(matchingMark.valuePct);
    } else {
      // Handle the case when there's no matching value
    }
  };

  /* Menu */
  const handleMenuClick = async () => {
    setIsDownloaded(false);
    if (state === "toGEN") {
      setState("GEN");
      setIsDlAble(false);
      const report = await APIsocket.generateReport(
        rate,
        selected,
        Date1,
        Date2
      );
      const settings = report.report_Settings;
      loadCurrentSettings(
        settings.state,
        settings.selected,
        settings.rate,
        settings.startDate,
        settings.endDate
      );
      setInitialData(report.report);
      setIsDlAble(true);
      setState("toGEN");
    } else if (state === "toREC") {
      setState("REC");
      const settings = await APIsocket.startReport(rate, selected);
      loadCurrentSettings(settings.state, settings.selected, settings.rate);
      setIsDlAble(true);
    } else if (state === "REC") {
      setState("toREC");
      const report = await APIsocket.stopReport();
      const settings = report.report_Settings;
      loadCurrentSettings(settings.state, settings.selected, settings.rate);
      setInitialData(report.report);
      setIsDlAble(true);
    }
  };
  const handleMenuItemClick = (event, index) => {
    setSelectedIndex(index);
    setState(menuOptions[index] === "GENERATE" ? "toGEN" : "toREC");
    setMenuOpen(false);
  };
  const handleMenuToggle = () => {
    setMenuOpen((prevOpen) => !prevOpen);
  };
  const handleClose = (event) => {
    if (anchorRefMenu.current && anchorRefMenu.current.contains(event.target)) {
      return;
    }

    setMenuOpen(false);
  };

  /* TREE SELECTOR */
  const handleTreeSelectorChange = (newValue) => {
    setSelected(newValue);
  };

  /* SLIDER */
  const valueLabelFormat = (value) => {
    return marks.find((mark) => mark.valuePct === value).label;
  };
  const handleSliderChange = (newValue) => {
    setRateRange(newValue);

    const matchingMark = marks.find((mark) => mark.valuePct === newValue);

    if (matchingMark) {
      setRate(matchingMark.valueMn);
    } else {
      // Handle the case when there's no matching value
    }
  };

  /* DATE RANGE */
  const handleDateRangeChange = (newValue) => {
    setDate1(newValue[0]);
    setDate2(newValue[1]);
  };

  /* EXPORT */
  const convertJsonToCsv = (data) => {
    // Extract the values and signals arrays
    const values = data.values;
    const signals = data.signals;

    // Add headers as the first row in the CSV
    const csvData = [signals].concat(values);

    // Convert values to CSV format
    const csv = csvData.map((row) => row.join(",")).join("\n");

    // Create a Blob
    const blob = new Blob([csv], { type: "text/csv" });
    const url = URL.createObjectURL(blob);

    // Create a download link
    const a = document.createElement("a");
    a.href = url;
    const D1 = dayjs.utc(values[0][0]).format("YYYY-MM-DD-T-HH-mm-ss"); // Replace with your Date1 logic
    const D2 = dayjs.utc(values[values.length-1][0]).format("YYYY-MM-DD-T-HH-mm-ss"); // Replace with your Date2 logic
    a.download = "Report_-_" + D1 + "_-_" + D2 + ".csv";
    a.click();

    // Cleanup after the download is complete
    URL.revokeObjectURL(url);
    setTimeout(() => {
      // Perform your action here (e.g., show a message)
      console.log("Download complete!");
    }, 1000); // 1000 milliseconds = 1 second
  };

  const handleSaveClick = async () => {
    /* Saves data */
    setIsDlLoading(true);
    try {
      const reportData = await APIsocket.getReport();
      convertJsonToCsv(reportData);
    } catch (error) {
      console.error("Error fetching event data:", error);
    }
    setIsDlLoading(false);
    setIsDownloaded(true);
  };

  const fetchReportData = async () => {
    try {
      const reportData = await APIsocket.getReport();
      setInitialData(reportData);
    } catch (error) {
      console.error("Error fetching event data:", error);
    }
  };
  const fetchReportSettings = async () => {
    try {
      const reportSettings = await APIsocket.getReportSettings();
      loadCurrentSettings(
        reportSettings.state,
        reportSettings.selected,
        reportSettings.rate,
        reportSettings.startDate,
        reportSettings.endDate
      );
    } catch (error) {
      console.error("Error fetching event data:", error);
    }
  };
  useEffect(() => {
    if (!hasLoaded) {
      setHasLoaded(true);
      fetchReportData();
      fetchReportSettings();
    }
    if (loggedIn.loggedIn && loggedIn.access === "RW") {
      setHasAccess(true);
    } else {
      setHasAccess(false);
    }

    if (state === "GEN") {
      setMenuButtonTxt("Generating Report");
      setMenuButtonTheme("info");
      setMenuButtonIcon(<CircularProgress size={"1rem"} />);
    } else if (state === "toGEN") {
      setMenuButtonTxt("Generate Report");
      setMenuButtonIcon(null);
      setMenuButtonTheme("success");
    } else if (state === "REC") {
      setMenuButtonTxt("Stop Recording");
      setMenuButtonIcon(RecordIcon);
      setMenuButtonTheme("error");
    } else if (state === "toREC") {
      setMenuButtonTxt("Start Recording");
      setMenuButtonIcon(null);
      setMenuButtonTheme("success");
    }
    const interval = setInterval(() => {
      if (isRecFirstIcon) {
        setRecordIcon(<RadioButtonUncheckedOutlinedIcon />);
      } else {
        setRecordIcon(<RadioButtonCheckedOutlinedIcon />);
      }
      setIsRecFirstIcon(!isRecFirstIcon);
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, [state, isRecFirstIcon, RecordIcon, hasLoaded]);

  return (
    <Box margin="normal" m="0.5rem 0 1rem 2rem">
      <Header
        title="Settings"
        subtitle="Configure Data, Connectivity and updates"
      />
      <Box /* Reports */
        display="flex"
        flexDirection="column"
        gap="0.5rem"
        mt="1rem"
      >
        <Box
          /* Reports - Params */ display="flex"
          flexDirection="column"
          gap="0.75rem"
        >
          <Box
            /* Reports - Params */ display="flex"
            gap="0.5rem"
            alignItems="center"
          >
            <Box /* Reports - Params - Freq */ width="25%" mr="1rem">
              <Slider
                valueLabelFormat={valueLabelFormat}
                step={null}
                value={rateRange}
                valueLabelDisplay="auto"
                disabled={!hasAccess || state === "REC"}
                marks={tickMarks}
                onChange={(event, newValue) => {
                  handleSliderChange(newValue);
                }}
                color="secondary"
              />
            </Box>
            <Box>
              <ButtonGroup
                variant="contained"
                ref={anchorRefMenu}
                aria-label="split button"
                disabled={state === "GEN" || !hasAccess}
              >
                <Button
                  onClick={handleMenuClick}
                  endIcon={menuButtonIcon}
                  color={menuButtonTheme}
                  disabled={
                    (state === "toGEN" &&
                      !(Date1 && Date2 && selected.length > 0)) ||
                    (state === "toREC" && !(selected.length > 0))
                  }
                >
                  {menuButtonTxt}
                </Button>
                <Button
                  size="small"
                  aria-controls={menuOpen ? "split-button-menu" : undefined}
                  aria-expanded={menuOpen ? "true" : undefined}
                  aria-label="select generation strategy"
                  aria-haspopup="menu"
                  onClick={handleMenuToggle}
                  disabled={state === "REC"}
                >
                  <ArrowDropDownIcon />
                </Button>
              </ButtonGroup>
              <Popper
                sx={{
                  zIndex: 1,
                }}
                open={menuOpen}
                anchorEl={anchorRefMenu.current}
                role={undefined}
                transition
                disablePortal
              >
                {({ TransitionProps, placement }) => (
                  <Grow
                    {...TransitionProps}
                    style={{
                      transformOrigin:
                        placement === "bottom" ? "center top" : "center bottom",
                    }}
                  >
                    <Paper>
                      <ClickAwayListener onClickAway={handleClose}>
                        <MenuList id="split-button-menu" autoFocusItem>
                          {menuOptions.map((option, index) => (
                            <MenuItem
                              key={option}
                              disabled={index === 2}
                              selected={index === selectedIndex}
                              onClick={(event) =>
                                handleMenuItemClick(event, index)
                              }
                            >
                              {option === "GENERATE"
                                ? "Generate Report"
                                : "Record Report"}
                            </MenuItem>
                          ))}
                        </MenuList>
                      </ClickAwayListener>
                    </Paper>
                  </Grow>
                )}
              </Popper>
            </Box>

            <TreeSelector
              rate={"LR"}
              Reset={0}
              ifChange={handleTreeSelectorChange}
              updateSelected={selected}
              disabled={state === "REC"}
            />
          </Box>
          {(state === "toGEN" || state === "GEN") && (
            <Box display="flex" gap="0.75rem" alignItems="center">
              <Box>
                <RangeDatePicker
                  ifChange={handleDateRangeChange}
                  updateDates={[Date1, Date2]}
                />
              </Box>
            </Box>
          )}
        </Box>
        <Box
          /* Reports - Table */ display="flex"
          flexDirection="column"
          width="100%"
          pr="2rem"
        >
          <Box
            display="inline-flex"
            gap="0.25rem"
            alignItems="center"
            ml="-0.5rem"
          >
            <Button /*Save Data*/
              variant="contained"
              endIcon={
                isDlLoading ? (
                  <CircularProgress size={"1rem"} />
                ) : isDownloaded ? (
                  <FileDownloadDoneOutlined />
                ) : (
                  <FileDownloadOutlined />
                )
              }
              color={isDlAble ? "secondary" : "error"}
              disabled={!isDlAble}
              onClick={handleSaveClick}
            >
              Save the Report
            </Button>
          </Box>
          <ReportTable initialData={initialData} />
        </Box>
      </Box>
    </Box>
  );
};

export default Report;
