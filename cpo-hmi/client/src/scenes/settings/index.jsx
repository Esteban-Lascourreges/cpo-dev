import React, { useEffect, useState } from "react";
import { Box, Typography, useTheme } from "@mui/material";

import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import CircularProgress from "@mui/material/CircularProgress";

import Header from "components/Header";
import APIsocket from "APIsocket";
import { loggedIn } from "scenes/layout";

const Settings = () => {
  useTheme();

  const [hasLoaded, setHasLoaded] = useState(false);
  const [hasAccess, setHasAccess] = useState(false);

  const [params, setParams] = useState({});

  const [realTimeUpdatePeriod, setRealTimeUpdatePeriod] = useState(15);
  const [availableSignalsUpdatePeriod, setAvailableSignalsUpdatePeriod] =
    useState(7);

  const [LrAPIurl, setLrAPIurl] = useState("http://10.39.100.10:3003/");
  const [HrAPIurl, setHrAPIurl] = useState("http://127.0.0.1:8000/");

  const [isLrAPIurlValid, setIsLrAPIurlValid] = useState(true);
  const [isHrAPIurlValid, setIsHrAPIurlValid] = useState(true);

  const [isSaveValid, setIsSaveValid] = useState(false);
  const [isSaveLoading, setIsSaveLoading] = useState(false);
  const [isCancelValid, setIsCancelValid] = useState(false);
  const [isDefaultValid, setIsDefaultValid] = useState(false);

  const isValidURL = (input) => {
    const pattern = /^(ftp|http|https):\/\/[^ "]+$/;
    return pattern.test(input);
  };

  const loadParams = (settings) => {
    setLrAPIurl(settings.LrAPIurl);
    setHrAPIurl(settings.HrAPIurl);
    setRealTimeUpdatePeriod(Number(settings.realTimeUpdatePeriod));
    setAvailableSignalsUpdatePeriod(Number(settings.availableSignalsUpdatePeriod));

    const newSettings = {
      LrAPIurl: settings.LrAPIurl,
      HrAPIurl: settings.HrAPIurl,
      realTimeUpdatePeriod: Number(settings.realTimeUpdatePeriod),
      availableSignalsUpdatePeriod: Number(settings.availableSignalsUpdatePeriod),
    };
    setParams(newSettings);
  };

  const handleRealTimeUpdatePeriodChange = (newValue) => {
    if (newValue < 1) {
      newValue = 1;
    }
    setRealTimeUpdatePeriod(Number(newValue));
  };
  const handleAvailableUpdatePeriodChange = (newValue) => {
    if (newValue < 1) {
      newValue = 1;
    }
    setAvailableSignalsUpdatePeriod(Number(newValue));
  };

  const handleLrAPIurlChange = (newValue) => {
    setLrAPIurl(newValue);
  };
  const handleHrAPIurlChange = (newValue) => {
    setHrAPIurl(newValue);
  };

  const handleSaveClick = async () => {
    /* Saves data */
    setIsSaveLoading(true);
    const newSettings = await APIsocket.setSettings(
      HrAPIurl,
      LrAPIurl,
      realTimeUpdatePeriod,
      availableSignalsUpdatePeriod
    );
    setParams(newSettings);
    loadParams(newSettings);
    setIsSaveLoading(false);
    console.log("Save");
  };

  const handleCancelClick = () => {
    /* Cancels changes */
    setAvailableSignalsUpdatePeriod(params.availableSignalsUpdatePeriod);
    setRealTimeUpdatePeriod(params.realTimeUpdatePeriod);
    setHrAPIurl(params.HrAPIurl);
    setLrAPIurl(params.LrAPIurl);
    console.log("Cancel");
  };

  const handleDefaultClick = () => {
    /* Cancels changes */
    setHrAPIurl("http://127.0.0.1:8000/");
    setLrAPIurl("http://10.39.100.10:3003/");
    setRealTimeUpdatePeriod(15);
    setAvailableSignalsUpdatePeriod(7);
    console.log("Default");
  };

  const fetchSettings = async () => {
    try {
      const response = await APIsocket.getSettings();
      loadParams(response);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (!hasLoaded) {
      setHasLoaded(true);
      fetchSettings();
    }
    if (loggedIn.loggedIn && loggedIn.access === "RW") {
      setHasAccess(true);
    } else {
      setHasAccess(false);
    }

    setIsHrAPIurlValid(isValidURL(HrAPIurl));
    setIsLrAPIurlValid(isValidURL(LrAPIurl));

    /* No Changes */
    if (
      params.LrAPIurl === LrAPIurl &&
      params.HrAPIurl === HrAPIurl &&
      params.realTimeUpdatePeriod === realTimeUpdatePeriod &&
      params.availableSignalsUpdatePeriod === availableSignalsUpdatePeriod
    ) {
      setIsCancelValid(false);
    } else {
      setIsCancelValid(true);
    }

    /* No default */
    if (
      LrAPIurl === "http://10.39.100.10:3003/" &&
      HrAPIurl === "http://127.0.0.1:8000/" &&
      realTimeUpdatePeriod === 15 &&
      availableSignalsUpdatePeriod === 7
    ) {
      setIsDefaultValid(false);
    } else {
      setIsDefaultValid(true);
    }

    /* Not Valid */
    if (isHrAPIurlValid && isLrAPIurlValid) {
      setIsSaveValid(isCancelValid);
    } else {
      setIsSaveValid(false);
    }
  }, [
    LrAPIurl,
    HrAPIurl,
    realTimeUpdatePeriod,
    availableSignalsUpdatePeriod,
    params,
    isHrAPIurlValid,
    isLrAPIurlValid,
    isCancelValid,
    isDefaultValid,
    isSaveValid,
    hasLoaded,
  ]);

  return (
    <Box margin="normal" m="0.5rem 0 1rem 2rem">
      <Header
        title="Settings"
        subtitle="Configure Data, Connectivity and updates"
      />
      <Box /* Settings */
        display="flex"
        flexDirection="column"
        gap="0.5rem"
        mt="1rem"
      >
        <Box /* Data Rates */>
          <Typography variant="h5">Data Rates Settings:</Typography>
          <Box display="flex" gap="0.25rem">
            <Box>
              <TextField /* Real-Time Update Period */
                size="small"
                id="RTupdate"
                label="Real-time update period (minutes)"
                InputLabelProps={{
                  shrink: true,
                }}
                type="number"
                value={realTimeUpdatePeriod}
                variant="filled"
                disabled={!hasAccess}
                onChange={(event) =>
                  handleRealTimeUpdatePeriodChange(event.target.value)
                }
              />
            </Box>
            <Box>
              <TextField /* Available signals */
                size="small"
                id="AvailableUpdate"
                label="Update Period (day(s))"
                InputLabelProps={{
                  shrink: true,
                }}
                type="number"
                disabled={!hasAccess}
                value={availableSignalsUpdatePeriod}
                variant="filled"
                onChange={(event) =>
                  handleAvailableUpdatePeriodChange(event.target.value)
                }
              />
            </Box>
          </Box>
        </Box>
        <Box /* address */>
          <Typography variant="h5">Data locations:</Typography>
          <Box display="flex" gap="0.25rem">
            <TextField
              size="small"
              id="HrAPIaddr"
              label="High-Rate API address"
              value={HrAPIurl}
              disabled={!hasAccess}
              error={!isHrAPIurlValid}
              variant="filled"
              onChange={(event) => handleHrAPIurlChange(event.target.value)}
            />
            <TextField
              size="small"
              id="LrAPIaddr"
              label="Low-Rate API address"
              value={LrAPIurl}
              disabled={!hasAccess}
              error={!isLrAPIurlValid}
              variant="filled"
              onChange={(event) => handleLrAPIurlChange(event.target.value)}
            />
          </Box>
        </Box>
        <Box>
          <Button /*Save Data*/
            color="success"
            disabled={
              !hasAccess || !isSaveValid || !isHrAPIurlValid || !isLrAPIurlValid
            }
            onClick={handleSaveClick}
            endIcon={isSaveLoading ? <CircularProgress size={"1rem"} /> : ""}
          >
            Save
          </Button>
          <Button /*Cancel*/
            color="error"
            disabled={!isCancelValid}
            onClick={handleCancelClick}
          >
            Cancel
          </Button>
          <Button /*Default*/
            color="secondary"
            disabled={!isDefaultValid || !hasAccess}
            onClick={handleDefaultClick}
          >
            Default
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default Settings;
