import React, { useEffect, useState } from "react";
import { Box, Typography, useTheme } from "@mui/material";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from "@mui/material";
import { Button, ButtonGroup } from "@mui/material";

import APIsocket from "APIsocket";
import Header from "components/Header";
import UserTable from "components/UserTable";
import { loggedIn } from "scenes/layout";

const SystemInfo = () => {
  const MuiTheme = useTheme();
  const colors = MuiTheme.palette;
  const sysPCsColumnNames = [
    "IP Address",
    "Name",
    "Version",
    "Status",
    "Uptime (Seconds)",
  ];
  const [hasLoaded, setHasLoaded] = useState(false);
  const [sysPCs, setSysPCs] = useState([]);
  const [sysJobs, setSysJobs] = useState([]);
  const [confirmationOpen, setConfirmationOpen] = useState(false);
  const [taskIDToDelete, setTaskIDToDelete] = useState("");
  const [hasAccess, setHasAccess] = useState(false);

  const handleStop = (taskID) => {
    // Open the confirmation dialog
    setTaskIDToDelete(taskID);
    setConfirmationOpen(true);
  };
  const confirmStop = async () => {
    // Add your logic for stopping the task here
    console.log(`Stop ${taskIDToDelete}`);
    try {
      const response = await APIsocket.postSuperStop(taskIDToDelete);
      setSysPCs(response.remote);
      setSysJobs(response.local);
    } catch (error) {
      console.log(error);
    }
    // Close the confirmation dialog
    setConfirmationOpen(false);
  };
  const cancelStop = () => {
    // Close the confirmation dialog without stopping the task
    setConfirmationOpen(false);
  };
  const handleStart = async (taskID) => {
    // Add your logic for starting the task here
    console.log(`Start ${taskID}`);
    try {
      const response = await APIsocket.postSuperStart(taskID);
      setSysPCs(response.remote);
      setSysJobs(response.local);
    } catch (error) {
      console.log(error);
    }
  };
  const handlePause = async (taskID) => {
    // Add your logic for pausing the task here
    console.log(`Pause ${taskID}`);
    try {
      const response = await APIsocket.postSuperPause(taskID);
      setSysPCs(response.remote);
      setSysJobs(response.local);
    } catch (error) {
      console.log(error);
    }
  };

  const fetchSysInfo = async () => {
    try {
      const response = await APIsocket.getSuper();
      setSysPCs(response.remote);
      setSysJobs(response.local);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (!hasLoaded) {
      setHasLoaded(true);
      fetchSysInfo();
    }
    const intervalId = setInterval(() => {
      fetchSysInfo(); // Fetch report data every 60 seconds
    }, 60000); // 60,000 milliseconds = 60 seconds

    if (loggedIn.loggedIn && loggedIn.access === "RW") {
      setHasAccess(true);
    } else {
      setHasAccess(false);
    }
    // Clear the interval on component unmount
    return () => {
      clearInterval(intervalId);
    };
  }, [hasLoaded]);

  return (
    <Box m="0.5rem 0 1rem 2rem" /* Page */>
      <Header
        title="System Info"
        subtitle="See running tasks and system information, You have the ability to start, stop and pause tasks for THIS hmi only."
      />
      <Box mt="0.5rem" display="flex" flexDirection="row" gap="1rem">
        <Box
          display="flex"
          flexDirection="column"
          gap="1rem"
          width="fit-content" /* Content */
        >
          <Box /* C4 */>
            <Typography variant="h5">
              System Info for C4 infrastructure (LPC0, CPC0, DPC0)
            </Typography>
            <Box>
              <TableContainer component={Paper}>
                <Table size="small">
                  <TableHead>
                    <TableRow>
                      {sysPCsColumnNames.map((columnName) => (
                        <TableCell key={columnName} style={{ minWidth: 120 }}>
                          {columnName}
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {Object.keys(sysPCs).map((ipAddress) =>
                      sysPCs[ipAddress].map((item, itemIndex) => (
                        <TableRow key={itemIndex}>
                          {itemIndex === 0 && (
                            <TableCell
                              rowSpan={sysPCs[ipAddress].length}
                              style={{ minWidth: 120 }}
                            >
                              {ipAddress}
                            </TableCell>
                          )}
                          <TableCell style={{ minWidth: 120 }}>
                            {item.name}
                          </TableCell>
                          <TableCell style={{ minWidth: 120 }}>
                            {item.version}
                          </TableCell>
                          <TableCell
                            style={{
                              minWidth: 120,
                              color:
                                item.status === "RUNNING"
                                  ? colors.success.main
                                  : colors.error.main,
                            }}
                          >
                            {item.status}
                          </TableCell>
                          <TableCell style={{ minWidth: 120 }}>
                            {item.uptimeInSeconds}
                          </TableCell>
                        </TableRow>
                      ))
                    )}
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
          </Box>
          <Box /* IHM */>
            <Typography variant="h5">
              Different tasks running on this HMI
            </Typography>
            <Box
              className="job-tables"
              display="flex"
              width="fit-content"
              flexDirection="column"
            >
              <TableContainer component={Paper}>
                <Table size="small">
                  <TableHead>
                    <TableRow>
                      <TableCell>Task Name</TableCell>
                      <TableCell>Task ID</TableCell>
                      <TableCell>Next Run Time</TableCell>
                      <TableCell>Trigger</TableCell>
                      <TableCell>Actions</TableCell>
                    </TableRow>
                  </TableHead>
                  {sysJobs.map((item, index) => (
                    <TableBody key={index}>
                      <TableRow key={index}>
                        <TableCell>{item.name}</TableCell>
                        <TableCell>{item.id}</TableCell>
                        <TableCell>{item.next_run_time}</TableCell>
                        <TableCell>{item.trigger}</TableCell>
                        <TableCell>
                          <ButtonGroup variant="outlined">
                            <Button
                              onClick={() => handleStart(item.id)}
                              color="success"
                              disabled={
                                !hasAccess || item.next_run_time !== "None"
                              }
                            >
                              Start
                            </Button>
                            <Button
                              onClick={() => handleStop(item.id)}
                              color="error"
                              disabled={!hasAccess}
                            >
                              Stop
                            </Button>
                            <Button
                              onClick={() => handlePause(item.id)}
                              color="warning"
                              disabled={
                                !hasAccess || item.next_run_time === "None"
                              }
                            >
                              Pause
                            </Button>
                          </ButtonGroup>
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  ))}
                </Table>
              </TableContainer>
              {/* Confirmation Dialog */}
              <Dialog open={confirmationOpen} onClose={cancelStop}>
                <DialogTitle>Confirm Stop</DialogTitle>
                <DialogContent>
                  <Typography>
                    Are you sure you want to stop the task "{taskIDToDelete}"?
                    This action cannot be undone, and the task will not be able
                    to be restarted later.
                  </Typography>
                </DialogContent>
                <DialogActions>
                  <Button
                    onClick={cancelStop}
                    color="secondary"
                    variant="outlined"
                  >
                    Cancel
                  </Button>
                  <Button
                    onClick={confirmStop}
                    color="error"
                    variant="contained"
                  >
                    Confirm
                  </Button>
                </DialogActions>
              </Dialog>
            </Box>
          </Box>
        </Box>
        <Box /* UserInfo */>
          <UserTable />
        </Box>
      </Box>
    </Box>
  );
};

export default SystemInfo;
