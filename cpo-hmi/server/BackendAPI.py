from fastapi import FastAPI, Query
from fastapi.responses import JSONResponse, PlainTextResponse
from fastapi.middleware.cors import CORSMiddleware
from pythonping import ping
import requests

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger

from datetime import datetime, timedelta

import json
import os
import re

API_Setting = {
    "HrAPIurl": "http://10.37.101.80:3003/",
    "LrAPIurl": "http://10.39.100.10:3003/",
    "realTimeUpdatePeriod": 15,
    "availableSignalsUpdatePeriod": 7
}
PCs = {
    "cpc0": "10.23.10.2",
    "dpc0": "10.23.10.3",
    "lpc0": "10.39.100.10",
}

report_Setting = {
    "state": "toGEN",
    "selected": [],
    "rate": 15,
    "startDate": "",
    "endDate": ""
}
report = {
    "signals": [],
    "values": []
}

sessions = []

queryables = {
    "signals": {
        "HighRate": {},
        "LowRate": {}
    }
}

PingInfo = {}

EventsData = {
    "CPC": [],
    "DPC": []
}

bus_info_total = json.load(open("./bus_info_total_ref.json", "r"))
signals_info = {
    "CCOR": bus_info_total["Bus"]["CCOR"],
    "DCOR": bus_info_total["Bus"]["DCOR"]
}

app = FastAPI(
    title="Monitoring HMI - Backend API",
    description="This is the backend API for the Monitoring HMI. It is used to get data from the High-Rate and Low-Rate APIs.",
    version="0.1.0",
    docs_url="/",
)
app.add_middleware(
    CORSMiddleware,
    # You might want to restrict this to your client's actual domain
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)

scheduler = BackgroundScheduler()
scheduler.start()


@app.get("/login", tags=["Management"], responses={
    200: {
        "description": "Successful Login with CorPower Ocean email",
        "content": {
            "application/json": {
                "example": {
                    "username": "admin",
                    "role": "admin",
                    "access": "RW"
                }
            }
        }
    },
    400: {
        "description": "Bad Request. Please check your credentials."
    },
})
async def login(
    username: str = Query(..., title="Username", description="Username"),
):
    credentials = json.loads(
        open(os.path.join("./", 'users.json'), 'r').read())['byEmail']
    for Users in credentials:
        if Users['username'] == username:
            response = {
                "username": Users['username'],
                "role": Users['role'],
                "access": Users['access']
            }
            return JSONResponse(content=response, media_type="application/json")
    return PlainTextResponse("Bad Request. Please check your credentials.", status_code=400)


@app.post("/session", tags=["Management"], responses={
    200: {
        "description": "New session created",
        "content": {
            "application/json": {
                "example": {
                    "username": "admin",
                    "role": "admin",
                    "sessionID": "1234567890",
                }
            }
        }
    },
})
async def create_session(
    username: str = Query(..., example="admin",
                          title="Username", description="Email"),
    role: str = Query(..., example="admin", title="Role", description="Role"),
    sessionID: str = Query(..., example="1234567890",
                           title="Session ID", description="Session ID"),
):
    sessions.append({
        "username": username,
        "role": role,
        "sessionID": sessionID,
    })
    return JSONResponse(content={"username": username, "role": role, "sessionID": sessionID}, media_type="application/json")


@app.delete("/session", tags=["Management"])
async def delete_session(
    sessionID: str = Query(..., example="1234567890")
):
    for index, session in enumerate(sessions):
        if sessionID == session["sessionID"]:
            sessions.pop(index)
    return JSONResponse(content=sessions, media_type="application/json")


@app.get("/users", tags=["Management"])
async def getUsers():
    unique_ID = []
    for index, session in enumerate(sessions):
        session_id = session["sessionID"]
        if session_id not in unique_ID:
            unique_ID.append(session_id)
        else:
            sessions.pop(index)
    return JSONResponse(content=sessions, media_type="application/json")


@app.get("/signals", tags=["Signals"], responses={
    200: {
        "description": "Successfully retrieved signals.",
        "content": {
            "application/json": {
                "example": {
                    "time": ["2023-10-18T00:00:00.000", "2023-10-18T00:00:00.010"],
                    "CCOR_SC_GBX.sc_gbx_rack_velocity": [0.25, 0.75],
                    "CCOR_SC_GBX.sc_gbx_rack_position": [3, 4]
                }
            }
        }
    },
    400: {
        "description": "Bad Request. For data from both CCOR and DCOR, please send two separate queries when using the Low-Rate API.<br>Bad Request. Please check your credentials.",
    }
})
async def getSignals(
    rate: str = Query(..., title="Select a Signal Rate",
                      description="Select a rate from the dropdown.", enum=["HR", "LR"]),
    signalFullNames: list[str] = Query(..., example=["CCOR_SC_GBX.sc_gbx_rack_position", "CCOR_SC_GBX.sc_gbx_rack_velocity"],
                                       title="Signal Names", description="Array of signal names in the **portName.signalName** format. <br>***N.B.*** When requesting data from both CCOR and DCOR it will fill the missing data with 'Null'"),
    startDateTime: str = Query(..., example="2023-10-22T12:15:00",
                               title="Start Time", description="Beginning of the requested time series as an [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) date time string"),
    endDateTime: str = Query(None, example="2023-10-22T12:15:00.5",
                             title="End Time", description="Optional: End of the requested time series as an ISO 8601 date time string. If omitted, it will be queried up to 5 minutes after the start time or the last entry. <br>*N.B.* It cannot be more than 30 min after the start time when using the High-Rate API."),
):
    startDateTime = datetime.fromisoformat(startDateTime)
    if endDateTime is None:
        endDateTime = datetime.fromisoformat(
            startDateTime)+timedelta(minutes=30)
    else:
        endDateTime = datetime.fromisoformat(endDateTime)
    if endDateTime > datetime.utcnow():
        endDateTime = datetime.utcnow()
    if rate == "HR":
        if (endDateTime-startDateTime) > timedelta(minutes=30):
            return PlainTextResponse("Bad Request. Time period cannot be more than 30 minutes when using the High-Rate API.", status_code=400)
        parameters = {
            "signalFullNames": signalFullNames,
            "startDateTime": startDateTime,
            "endDateTime": endDateTime,
        }
        headers = {'Accept': 'application/json'}
        response = requests.get(
            API_Setting["HrAPIurl"]+"signals", params=parameters, headers=headers)
        signals = response.json()
    elif rate == "LR":
        isCCOR = False
        isDCOR = False
        for signal in signalFullNames:
            if "CCOR" in signal:
                isCCOR = True
            elif "DCOR" in signal:
                isDCOR = True
        if isCCOR and isDCOR:
            return PlainTextResponse("Bad Request. For data from both CCOR and DCOR, please send two separate queries.", status_code=400)
        parameters = {
            "signalFullNames": signalFullNames,
            "startDateTime": startDateTime,
            "endDateTime": endDateTime,
        }
        headers = {'Accept': 'application/json'}
        response = requests.get(
            API_Setting["LrAPIurl"]+"signals", params=parameters, headers=headers)
        signals = response.json()

    outputSignals = {}
    for index, key in enumerate(signals["signals"]):
        if key == "SW_version":
            continue
        splitted = key.split(".")[:2]
        if len(splitted) > 1:
            port, signal_name = splitted
            if "DCOR" in port or "DPC" in port:
                unit_signal_name = f"{port}.{signal_name} ({signals_info['DCOR'][port][signal_name]['unit']})"
            elif "CCOR" in port or "CPC" in port:
                unit_signal_name = f"{port}.{signal_name} ({signals_info['CCOR'][port][signal_name]['unit']})"
        else:
            unit_signal_name = key

        outputSignals[unit_signal_name] = []
        for values in signals["values"]:
            outputSignals[unit_signal_name].append(values[index])

    return JSONResponse(content=outputSignals, media_type="application/json")


@app.get("/queryables", tags=["Signals"], responses={
    200: {
        "description": "successfully retrieved signals.",
        "content": {
            "application/json": {
                "example": {
                    "signals": {
                        "HighRate": {
                            "CCOR": [
                                "CCOR_CMD.CMD_ccor_test_id",
                                "CCOR_CMD.CMD_cd_pes_CBR002_open",
                                "CCOR_CD_GBX.cd_gbx_teaching_procedure_state",
                                "CCOR_CD_GBX.cd_gbx_measured_stroke",
                                "CCOR_CD_GBX.cd_gbx_position_to_teach",
                                "CCOR_CD_GBX.cd_gbx_teach_position",
                                "CCOR_CD_GBX.cd_gbx_FRS101_piezo_pin_reset"
                            ],
                            "DCOR": [
                                "DCOR_CMD.CMD_ccor_test_id",
                                "DCOR_CMD.CMD_cd_pes_CBR002_open",
                                "DCOR_CD_GBX.cd_gbx_teaching_procedure_state",
                                "DCOR_CD_GBX.cd_gbx_measured_stroke",
                                "DCOR_CD_GBX.cd_gbx_position_to_teach",
                                "DCOR_CD_GBX.cd_gbx_teach_position",
                                "DCOR_CD_GBX.cd_gbx_FRS101_piezo_pin_reset"
                            ]
                        },
                        "LowRate": {
                            "CCOR": [
                                "CCOR_CMD.CMD_ccor_test_id",
                                "CCOR_CMD.CMD_cd_pes_CBR002_open",
                                "CCOR_CD_GBX.cd_gbx_teaching_procedure_state",
                                "CCOR_CD_GBX.cd_gbx_measured_stroke",
                                "CCOR_CD_GBX.cd_gbx_position_to_teach",
                                "CCOR_CD_GBX.cd_gbx_teach_position",
                                "CCOR_CD_GBX.cd_gbx_FRS101_piezo_pin_reset"
                            ],
                            "DCOR": [
                                "DCOR_CMD.CMD_ccor_test_id",
                                "DCOR_CMD.CMD_cd_pes_CBR002_open",
                                "DCOR_CD_GBX.cd_gbx_teaching_procedure_state",
                                "DCOR_CD_GBX.cd_gbx_measured_stroke",
                                "DCOR_CD_GBX.cd_gbx_position_to_teach",
                                "DCOR_CD_GBX.cd_gbx_teach_position",
                                "DCOR_CD_GBX.cd_gbx_FRS101_piezo_pin_reset"
                            ]
                        }
                    }
                }
            }
        }
    }
})
def getQueryables():
    parameters = {}
    headers = {'Accept': 'application/json'}
    response = requests.get(API_Setting["HrAPIurl"]+"queryables",
                            params=parameters, headers=headers)
    response = response.json()
    queryables["signals"]["HighRate"] = response["signals"]
    parameters = {
        "signals": 'true',
        "parameters": 'false',
        "script_outputs": 'false',
    }
    response = requests.get(API_Setting["LrAPIurl"]+"queryables",
                            params=parameters, headers=headers)
    response = response.json()
    queryables["signals"]["LowRate"] = response["signals"]

    return JSONResponse(content=queryables, media_type="application/json")


@app.get("/dataTree", tags=["Signals"], responses={
    200: {
        "description": "Successfully retrieved the available signals with the correct data tree structure.",
        "content": {
            "application/json": {
                "example": [
                    {
                        "title": "The Data Tree Needs/Is Being Generated",
                        "value": "VALUE1",
                        "selectable": "false",
                        "children": [
                                {
                                    "title": "Children 2",
                                    "value": "VALUE12",
                                    "selectable": "true",
                                },
                            {
                                    "title": "Children 2",
                                    "value": "VALUE12",
                                    "selectable": "true",
                                    }
                        ]
                    },
                    {
                        "title": "Please wait for the data to load",
                        "value": "VALUE2",
                        "selectable": "false",
                        "children": [
                            {
                                "title": "Children 1",
                                "value": "VALUE21",
                                "selectable": "true",
                            },
                            {
                                "title": "Children 2",
                                "value": "VALUE22",
                                "selectable": "true",
                            }
                        ]
                    }
                ]
            }
        }
    }
})
async def getDataTree(
    rate: str = Query(..., title="Select a Signal Rate",
                      description="Select a rate from the dropdown.", enum=["HR", "LR"]),
):
    global queryables
    if ((queryables["signals"]["HighRate"] == {}) or (queryables["signals"]["LowRate"] == {})):
        get_queryables = await getQueryables()
        queryables = get_queryables.json()
    response = {
        "signals": {
            "CCOR": [],
            "DCOR": []
        }
    }
    if rate == "HR":
        response["signals"]["CCOR"] = queryables["signals"]["HighRate"]["CCOR"]
        response["signals"]["DCOR"] = queryables["signals"]["HighRate"]["DCOR"]
    elif rate == "LR":
        response["signals"]["CCOR"] = queryables["signals"]["LowRate"]["CCOR"]
        response["signals"]["DCOR"] = queryables["signals"]["LowRate"]["DCOR"]

    CCOR = _create_category_structure(response["signals"]["CCOR"])
    DCOR = _create_category_structure(response["signals"]["DCOR"])
    dataTree = json.dumps(CCOR+DCOR)
    return JSONResponse(content=json.loads(dataTree), media_type="application/json")


@app.get("/report", tags=["Report Maker"], responses={
    200: {
        "description": "Successfully retrieved the report.",
        "content": {
            "application/json": {
                "example": {
                    "time": [
                        "2023-10-18T00:00:00.000",
                        "2023-10-18T00:30:00.000"
                    ],
                    "CCOR_SC_GBX.sc_gbx_rack_velocity": [
                        0.25,
                        0.75
                    ],
                    "CCOR_SC_GBX.sc_gbx_rack_position": [
                        3,
                        4
                    ]
                }
            }
        }
    }
})
async def getReport():
    return JSONResponse(content=report, media_type="application/json")


@app.get("/reportSettings", tags=["Report Maker"], responses={
    200: {
        "description": "Successfully retrieved the report settings.",
        "content": {
            "application/json": {
                "example": {
                    "state": "GEN",
                    "selected": ["CCOR_SC_GBX.sc_gbx_rack_velocity", "CCOR_SC_GBX.sc_gbx_rack_position"],
                    "rate": 15,
                    "startDate": "2023-10-18T00:00:00.000",
                    "endDate": "2023-10-18T15:00:00.000"
                }
            }
        }
    }
})
async def getReportSettings():
    return JSONResponse(content=report_Setting, media_type="application/json")


@app.post("/reportSettings", tags=["Report Maker"], responses={
    200: {
        "description": "Successfully set the report settings.",
        "content": {
            "application/json": {
                "example": {
                    "state": "GEN",
                    "selected": ["CCOR_SC_GBX.sc_gbx_rack_velocity", "CCOR_SC_GBX.sc_gbx_rack_position"],
                    "rate": 15,
                    "startDate": "2023-10-18T00:00:00.000",
                    "endDate": "2023-10-18T15:00:00.000"
                }
            }
        }
    }
})
async def setReportSettings(
    report_state: str = Query(..., title="Select a Report State",
                              description="Select a state from the dropdown.", enum=["toREC", "REC", "toGEN", "GEN"]),
    signalFullNames: list[str] = Query(..., example=["CCOR_SC_GBX.sc_gbx_rack_position", "CCOR_SC_GBX.sc_gbx_rack_velocity"],
                                       title="Signal Names", description="Array of signal names"),
    sampleRate: int = Query(..., example="15", title="Sample Rate",
                            description="Select sample rate in minutes"),
    startDateTime: str = Query(
        None, title="Start Time", description="Optional ISO 8601 format"),
    endDateTime: str = Query(None, title="End Time",
                             description="Optional ISO 8601 format"),
):
    report_Setting["state"] = report_state
    report_Setting["selected"] = signalFullNames
    report_Setting["rate"] = sampleRate
    report_Setting["startDate"] = startDateTime if startDateTime else ''
    report_Setting["endDate"] = endDateTime if endDateTime else ''
    return JSONResponse(content=report_Setting, media_type="application/json")


@app.post("/report/Start", tags=["Report Maker"], responses={
    200: {
        "description": "Successfully started the recording of a report.",
        "content": {
            "application/json": {
                "example": {
                    "report_Settings": {
                        "state": "REC",
                        "selected": ["CCOR_SC_GBX.sc_gbx_rack_velocity", "CCOR_SC_GBX.sc_gbx_rack_position"],
                        "rate": 15,
                        "startDate": '',
                        "endDate": ''
                    }
                }
            }
        }
    }
})
async def startReport(
    sampleRate: int = Query(..., example="15", title="Sample Rate",
                            description="Select sample rate in minutes"),
    signalFullNames: list[str] = Query(..., example=["CCOR_SC_GBX.sc_gbx_rack_position", "CCOR_SC_GBX.sc_gbx_rack_velocity"],
                                       title="Signal Names", description="Array of signal names"),
):
    if "report_job" in scheduler.get_jobs():
        return PlainTextResponse("Bad Request. A recording is already on going please stop it before starting a new one.", status_code=400)
    isCCOR = False
    isDCOR = False
    for signal in signalFullNames:
        if "CCOR" in signal:
            isCCOR = True
        elif "DCOR" in signal:
            isDCOR = True
    if isCCOR and isDCOR:
        return PlainTextResponse("Bad Request. For data from both CCOR and DCOR, please send two separate queries.", status_code=400)
    report_Setting["state"] = "REC"
    report_Setting["selected"] = signalFullNames
    report_Setting["rate"] = sampleRate
    report_Setting["startDate"] = ''
    report_Setting["endDate"] = ''
    report["signals"] = []
    report["values"] = []
    isRecording = False
    for job in scheduler.get_jobs():
        if job.id == "report_job":
            isRecording = True
    if isRecording:
        return PlainTextResponse("Bad Request. A recording is already on going please stop it before starting a new one.", status_code=400)
    scheduler.add_job(
        _new_report_line,
        IntervalTrigger(minutes=sampleRate/60),
        args=[signalFullNames],
        id="report_job",
        next_run_time=datetime.now(),
    )
    return JSONResponse(content=report_Setting, media_type="application/json")


@app.post("/report/Stop", tags=["Report Maker"], responses={
    200: {
        "description": "Successfully stopped the recording of a report.",
        "content": {
            "application/json": {
                "example": {
                    "report_Settings": {
                        "state": "toREC",
                        "selected": ["CCOR_SC_GBX.sc_gbx_rack_velocity", "CCOR_SC_GBX.sc_gbx_rack_position"],
                        "rate": 30,
                        "startDate": '',
                        "endDate": ''
                    },
                    "report": {
                        "time": [
                            "2023-10-18T00:00:00.000",
                            "2023-10-18T00:30:00.000"
                        ],
                        "CCOR_SC_GBX.sc_gbx_rack_velocity": [
                            0.25,
                            0.75
                        ],
                        "CCOR_SC_GBX.sc_gbx_rack_position": [
                            3,
                            4
                        ]
                    }
                }
            }
        }
    }
})
async def stopReport():
    isRecording = False
    for job in scheduler.get_jobs():
        if job.id == "report_job":
            isRecording = True
    if isRecording:
        scheduler.remove_job("report_job")
    report_Setting["state"] = "toREC"
    response = {
        "report_Settings": report_Setting,
        "report": report
    }
    return JSONResponse(content=response, media_type="application/json")


@app.post("/report/GEN", tags=["Report Maker"], responses={
    200: {
        "description": "Successfully generated a report.",
        "content": {
            "application/json": {
                "example": {
                    "report_Settings": {
                        "state": "toGEN",
                        "selected": ["CCOR_SC_GBX.sc_gbx_rack_velocity", "CCOR_SC_GBX.sc_gbx_rack_position"],
                        "rate": 15,
                        "startDate": "2023-10-18T00:00:00.000",
                        "endDate": "2023-10-18T00:20:00.000"
                    },
                    "report": {
                        "time": [
                            "2023-10-18T00:00:00.000",
                            "2023-10-18T00:20:00.000"
                        ],
                        "CCOR_SC_GBX.sc_gbx_rack_velocity": [
                            0.25,
                            0.75
                        ],
                        "CCOR_SC_GBX.sc_gbx_rack_position": [
                            3,
                            4
                        ]
                    }
                }
            }
        }
    }
})
async def generateReport(
    sampleRate: int = Query(..., example="15", title="Sample Rate",
                            description="Select sample rate in minutes"),
    signalFullNames: list[str] = Query(..., example=["CCOR_SC_GBX.sc_gbx_rack_position", "CCOR_SC_GBX.sc_gbx_rack_velocity"],
                                       title="Signal Names", description="Array of signal names"),
    startDateTime: str = Query(..., example="2023-10-22T12:00:00",
                               title="Start Time", description="Beginning of the requested report as an [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) date time string"),
    endDateTime: str = Query(..., example="2023-10-22T13:00:00",
                             title="End Time", description="End of the requested report as an [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) date time string"),
):

    report_Setting["state"] = "GEN"
    isCCOR = False
    isDCOR = False
    for signal in signalFullNames:
        if "CCOR" in signal:
            isCCOR = True
        elif "DCOR" in signal:
            isDCOR = True
    if isCCOR and isDCOR:
        return PlainTextResponse("Bad Request. For data from both CCOR and DCOR, please send two separate queries.", status_code=400)
    parameters = {
        "signalFullNames": signalFullNames,
        "startDateTime": startDateTime,
        "endDateTime": endDateTime,
    }
    headers = {'Accept': 'application/json'}
    response = requests.get(
        API_Setting["LrAPIurl"]+"signals", params=parameters, headers=headers)
    response = response.json()

    report["signals"] = []
    for key in response["signals"]:
        splitted = key.split(".")[:2]
        if len(splitted) > 1:
            port, signal_name = splitted
            if "DCOR" in port or "DPC" in port:
                unit_signal_name = f"{port}.{signal_name} ({signals_info['DCOR'][port][signal_name]['unit']})"
            elif "CCOR" in port or "CPC" in port:
                unit_signal_name = f"{port}.{signal_name} ({signals_info['CCOR'][port][signal_name]['unit']})"
        else:
            unit_signal_name = key

        report["signals"].append(unit_signal_name)

    report["values"] = []
    latestTime = datetime.fromisoformat('2000-01-01T00:00:00.000Z')
    for values in response["values"]:
        currentTime = datetime.fromisoformat(values[0])
        if currentTime - latestTime >= timedelta(minutes=sampleRate):
            latestTime = datetime.fromisoformat(values[0])
            report["values"].append(values)

    report_Setting["selected"] = signalFullNames
    report_Setting["rate"] = sampleRate
    report_Setting["startDate"] = startDateTime
    report_Setting["endDate"] = endDateTime

    response = {
        "report_Settings": report_Setting,
        "report": report
    }
    report_Setting["state"] = "toGEN"
    return JSONResponse(content=response, media_type="application/json")


@app.get("/settings", tags=["Settings"], responses={
    200: {
        "description": "Successfully retrieved the settings.",
        "content": {
            "application/json": {
                "example": {
                    "HrAPIurl": "http://10.37.101.80:3003/",
                    "LrAPIurl": "http://10.39.100.10:3003/",
                    "realTimeUpdatePeriod": 15,
                    "availableSignalsUpdatePeriod": 7,
                }
            }
        }
    }
})
async def getSettings():
    return JSONResponse(content=API_Setting, media_type="application/json")


@app.post('/settings', tags=["Settings"], responses={
    200: {
        "description": "Successfully set the settings.",
        "content": {
            "application/json": {
                "example": {
                    "HrAPIurl": "http://10.37.101.80:3003/",
                    "LrAPIurl": "http://10.39.100.10:3003/",
                    "realTimeUpdatePeriod": 15,
                    "availableSignalsUpdatePeriod": 7,
                }
            }
        }
    }
})
async def setSettings(
        new_HrAPIurl: str = Query(..., title="HighRate URL",
                                  description="High-Rate API url (http://10.10.10.10:8080/)"),
        new_LrAPIurl: str = Query(..., title="LowRate URL",
                                  description="Low-Rate API url (http://10.10.10.10:8181/)"),
        new_realTimeUpdatePeriod: str = Query(..., title="RealTime update period",
                                              description="How often the RealTime data is updated (in minutes)"),
        new_availableSignalsUpdatePeriod: str = Query(..., title="Available Signals update period",
                                                      description="How often the Available Signals data is updated (in days)"),
):
    API_Setting["HrAPIurl"] = assertURL(new_HrAPIurl)
    API_Setting["LrAPIurl"] = assertURL(new_LrAPIurl)
    API_Setting["realTimeUpdatePeriod"] = new_realTimeUpdatePeriod
    API_Setting["availableSignalsUpdatePeriod"] = new_availableSignalsUpdatePeriod
    return JSONResponse(content=API_Setting, media_type="application/json")


@app.get("/ping", tags=["Super"], responses={
    200: {
        "description": "Successfully pinged the IP address",
        "content": {
            "application/json": {
                "example": {
                    'verbose': 'Reply from 10.39.100.10, 4 packets, avg time=70.48 ms',
                    'details': {
                        'responses': [
                            "Reply from 10.39.100.10, 29 bytes in 79.67ms",
                            "Reply from 10.39.100.10, 29 bytes in 66.42ms",
                            "Reply from 10.39.100.10, 29 bytes in 62.22ms",
                            "Reply from 10.39.100.10, 29 bytes in 73.6ms"
                        ],
                        'packets_sent': 4,
                        'packets_returned': 4,
                        'rtt_avg': 0.07047957499980839,
                        'rtt_min': 0.06222399999933259,
                        'rtt_max': 0.07966599999963364
                    },
                    'averagePingTime': 70.48
                }
            }
        }
    }
})
def getPing(
    ip: str = Query(..., title="IP Address", description="IP Address"),
):
    try:
        ip = assertIPV4(ip)
    except AssertionError as e:
        return PlainTextResponse(str(e), status_code=400)

    response = ping(ip, count=4)
    i_p = ip.replace(".", "_")
    PingInfo[i_p] = {
        "verbose": "",
        "details": {
            "responses": [],
            "packets_sent": 0,
            "packets_returned": 0,
            "rtt_avg": 0,
            "rtt_min": 0,
            "rtt_max": 0,
        },
        "averagePingTime": 0
    }
    if response.success() and response.rtt_avg_ms:
        PingInfo[i_p]["verbose"] = f"Reply from {ip}, {response.stats_packets_returned} packets, avg time={response.rtt_avg_ms} ms"
        for val in response._responses:
            PingInfo[i_p]["details"]["responses"].append(val.__str__())
        PingInfo[i_p]["details"]["packets_sent"] = response.stats_packets_sent
        PingInfo[i_p]["details"]["packets_returned"] = response.stats_packets_returned
        PingInfo[i_p]["details"]["rtt_avg"] = response.rtt_avg
        PingInfo[i_p]["details"]["rtt_min"] = response.rtt_min
        PingInfo[i_p]["details"]["rtt_max"] = response.rtt_max
        PingInfo[i_p]["averagePingTime"] = response.rtt_avg_ms
        return JSONResponse(content=PingInfo[i_p], media_type="application/json")
    else:
        return PlainTextResponse("Bad Request. Please check the IP address.", status_code=400)


@app.get("/ping/info", tags=["Super"], responses={
    200: {
        "description": "Successfully pinged the IP address",
        "content": {
            "application/json": {
                "example": {
                    'verbose': 'Reply from 10.39.100.10, 4 packets, avg time=70.48 ms',
                    'details': {
                        'responses': [
                            "Reply from 10.39.100.10, 29 bytes in 79.67ms",
                            "Reply from 10.39.100.10, 29 bytes in 66.42ms",
                            "Reply from 10.39.100.10, 29 bytes in 62.22ms",
                            "Reply from 10.39.100.10, 29 bytes in 73.6ms"
                        ],
                        'packets_sent': 4,
                        'packets_returned': 4,
                        'rtt_avg': 0.07047957499980839,
                        'rtt_min': 0.06222399999933259,
                        'rtt_max': 0.07966599999963364
                    },
                    'averagePingTime': 70.48
                }
            }
        }
    }
})
async def getPingInfo(
    ip: str = Query(..., title="IP Address", description="IP Address"),
):
    try:
        ip = assertIPV4(ip)
    except AssertionError as e:
        return PlainTextResponse(str(e), status_code=400)
    i_p = ip.replace(".", "_")
    isPingJob = False
    for job in scheduler.get_jobs():
        if job.id == f"ping_job-{i_p}":
            isPingJob = True
            if ip not in list(PingInfo.keys()):
                getPing(ip)

    if not isPingJob:
        scheduler.add_job(
            getPing,
            IntervalTrigger(seconds=5),
            args=[ip],
            id=f"ping_job-{i_p}",
        )
        getPing(ip)
    return JSONResponse(content=PingInfo[i_p], media_type="application/json")


@app.get("/ping/info/all", tags=["Super"])
async def getPingInfoAll():
    return JSONResponse(content=PingInfo, media_type="application/json")


@app.get("/super", tags=["Super"], responses={
    200: {
        "description": "Successfully retrieved the supervisor data.",
        "content": {
            "application/json": {
                "example": {
                    "10.23.10.2": [
                        {
                            "name": "dataTransferDaemon",
                            "version": "1.06.00-rc18",
                            "status": "RUNNING",
                            "uptimeInSeconds": 387
                        },
                        {
                            "name": "lhss",
                            "version": "1.06.00-rc18",
                            "status": "RUNNING",
                            "uptimeInSeconds": 173956
                        }
                    ],
                    "10.23.10.3": [
                        {
                            "name": "dhsw",
                            "version": "1.06.00-rc18",
                            "status": "RUNNING",
                            "uptimeInSeconds": 173953
                        }
                    ],
                    "local": [
                        {
                            "name": "_new_report_line",
                            "next_run_time": "2023-10-26 11:10:02.099253+02:00",
                            "trigger": "interval[0:00:15]"
                        }
                    ]
                }
            }
        }
    }
})
async def getSuper():
    data = {
        'remote': {},
        'local': []
    }
    for key in list(PCs.keys()):
        ip = PCs[key]
        response = requests.get(
            f"{API_Setting['LrAPIurl']}super/{ip}", headers={'Accept': 'application/json'})
        data['remote'][ip] = response.json()
    for job in scheduler.get_jobs():
        data['local'].append({'name': job.name,'id': job.id, 'next_run_time': str(
            job.next_run_time), 'trigger': str(job.trigger)})
    return JSONResponse(content=data, media_type="application/json")


@app.post("/super/start", tags=["Super"])
async def postSuperStart(
    task: str = Query(..., examples="_new_report_line",
                      title="Task", description="Task to start"),
):
    for job in scheduler.get_jobs():
        if job.id == task:
            job.resume()
    return await getSuper()


@app.post("/super/pause", tags=["Super"])
async def postSuperPause(
    task: str = Query(..., examples="_new_report_line",
                      title="Task", description="Task to start"),
):
    for job in scheduler.get_jobs():
        if job.id == task:
            job.pause()
    return await getSuper()


@app.post("/super/stop", tags=["Super"])
async def postSuperStop(
    task: str = Query(..., examples="_new_report_line",
                      title="Task", description="Task to start"),
):
    for job in scheduler.get_jobs():
        if job.id == task:
            job.remove()
    return await getSuper()


@app.get("/realTime", tags=["Real Time"])
async def getRealTime():
    pass


@app.get("/events", tags=["Real Time"])
def getEvents():
    parameters = {
        "startDateTime": (datetime.utcnow() - timedelta(days=1)).isoformat(),
    }
    response = requests.get(f"{API_Setting['LrAPIurl']}events_timeline", params=parameters, headers={
                            'Accept': 'application/json'})
    response_data = response.json()
    global EventsData
    EventsData = {
        "CPC": [],
        "DPC": []
    }
    counters = {}
    for event in response_data["CPC"]:
        if event["id"] not in list(counters.keys()):
            counters[event["id"]] = {
                "start": event["start"],
                "end": event["end"],
                "count": 1,
                "severity": [event["severity"]],
                "id": event["id"]
            }
        else:
            counters[event["id"]]["count"] += 1
            if datetime.fromisoformat(counters[event["id"]]["start"])>datetime.fromisoformat(event["start"]):
                counters[event["id"]]["start"] = event["start"]
            if datetime.fromisoformat(counters[event["id"]]["end"])<datetime.fromisoformat(event["end"]):
                counters[event["id"]]["end"] = event["end"]
            if event["severity"] not in counters[event["id"]]["severity"]:
                counters[event["id"]]["severity"].append(event["severity"])
                
    for individual_event in list(counters.keys()):
        EventsData["CPC"].append({
            "id": f"{counters[individual_event]['id']} ({counters[individual_event]['count']})",
            "start": counters[individual_event]["start"],
            "end": counters[individual_event]["end"],
            "severity": counters[individual_event]["severity"]
        })
    counters = {}
    for event in response_data["DPC"]:
        if event["id"] not in list(counters.keys()):
            counters[event["id"]] = {
                "start": event["start"],
                "end": event["end"],
                "count": 1,
                "severity": [event["severity"]],
                "id": event["id"]
            }
        else:
            counters[event["id"]]["count"] += 1
            if datetime.fromisoformat(counters[event["id"]]["start"])>datetime.fromisoformat(event["start"]):
                counters[event["id"]]["start"] = event["start"]
            if datetime.fromisoformat(counters[event["id"]]["end"])<datetime.fromisoformat(event["end"]):
                counters[event["id"]]["end"] = event["end"]
            if event["severity"] not in counters[event["id"]]["severity"]:
                counters[event["id"]]["severity"].append(event["severity"])
    for individual_event in list(counters.keys()):
        EventsData["DPC"].append({
            "id": f"{counters[individual_event]['id']} ({counters[individual_event]['count']})",
            "start": counters[individual_event]["start"],
            "end": counters[individual_event]["end"],
            "severity": counters[individual_event]["severity"]
        })
    
    return JSONResponse(content=EventsData, media_type="application/json")


@app.get("/events/data", tags=["Real Time"])
async def getEventsData():
    isEventJob = False
    for job in scheduler.get_jobs():
        if job.id == "events_job":
            isEventJob = True
    if not isEventJob:
        scheduler.add_job(
            getEvents,
            IntervalTrigger(minutes=API_Setting["realTimeUpdatePeriod"]),
            id="events_job",
            next_run_time=datetime.now(),
        )
    return JSONResponse(content=EventsData, media_type="application/json")


def _handler_Real_Time():
    pass
    # parameters = {
    #    "startDateTime": (datetime.utcnow() - timedelta(days=1)).isoformat(),
    # }
    # global EventsData
    # event = requests.get(f"{API_Setting['LrAPIurl']}events_timeline", params=parameters,headers={'Accept': 'application/json'})
    # EventsData = event.json()


def _new_report_line(selected):
    startDate = datetime.utcnow() - timedelta(seconds=31)
    endDate = datetime.utcnow() - timedelta(seconds=30)
    parameters = {
        "signalFullNames": selected,
        "startDateTime": startDate,
        "endDateTime": endDate,
    }
    headers = {'Accept': 'application/json'}
    response = requests.get(
        API_Setting["LrAPIurl"] + "signals", params=parameters, headers=headers)
    signals = response.json()
    report["signals"] = []
    for key in signals["signals"]:
        splitted = key.split(".")[:2]
        if len(splitted) > 1:
            port, signal_name = splitted
            if "DCOR" in port or "DPC" in port:
                unit_signal_name = f"{port}.{signal_name} ({signals_info['DCOR'][port][signal_name]['unit']})"
            elif "CCOR" in port or "CPC" in port:
                unit_signal_name = f"{port}.{signal_name} ({signals_info['CCOR'][port][signal_name]['unit']})"
        else:
            unit_signal_name = key

        report["signals"].append(unit_signal_name)

    report["values"].append(signals["values"][-1])


def _create_category_structure(names):
    categories = []
    for name in names:
        parts = name.split('.')
        current_category = categories
        for i, part in enumerate(parts):
            full_name = '.'.join(parts[:i+1])
            existing_category = next(
                (c for c in current_category if c['title'] == part), None)
            if existing_category:
                current_category = existing_category.get('children', [])
            else:
                new_category = {
                    'title': part,
                    'value': full_name
                }
                if i < len(parts) - 1:
                    new_category['selectable'] = False
                    new_category['children'] = []
                current_category.append(new_category)
                current_category = new_category.get('children', [])
    return categories


def assertURL(_url):  # Assert URL
    try:
        assert isinstance(_url, str), "URL must be a string"
        assert len(_url) > 0, "URL must be a non-empty string"
        assert re.match(
            r'^(ftp|http|https)://[^ "]+/?$', _url), "URL must be a valid URL"
    except AssertionError as e:
        raise AssertionError(e)
    return _url


def assertIPV4(_ip):  # Assert IPV4
    try:
        pattern = r'^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'
        assert isinstance(_ip, str), "IP must be a string"
        assert len(_ip) > 0, "IP must be a non-empty string"
        assert re.match(pattern, _ip), "IP must be a valid IPV4 address"
        return _ip
    except AssertionError as e:
        raise AssertionError(e)

    return _ip


scheduler.add_job(getQueryables, IntervalTrigger(
    days=API_Setting["availableSignalsUpdatePeriod"]), id="queryables_job", next_run_time=datetime.now())
scheduler.add_job(getEvents, IntervalTrigger(
    minutes=API_Setting["realTimeUpdatePeriod"]), id="events_job", next_run_time=datetime.now())
# scheduler.add_job(_handler_Real_Time, IntervalTrigger(minutes=API_Setting["realTimeUpdatePeriod"]), id="realTime_job", next_run_time=datetime.now())
